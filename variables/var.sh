#!/bin/bash

touch manifests/variable.sh
chmod a+x manifests/variable.sh

cat > manifests/variable.sh <<EOF
echo ""
echo "Run variable.sh"
echo ""
# The variables we created when we ran setup.sh.
export vgrthostname=${vgrthostname}
export vgrtip=${vgrtip}
export vgrtserver=${vgrtserver}
export vgrtsite=${vgrtsite}
export vgrtwatiamid=${vgrtwatiamid}
export vgrtemail=${vgrtemail}

# Github token
export vgrtgithubtoken=${vgrtgithubtoken}

# Location of fonts folder (if entered)
export vgrtfontfolder=${vgrtfontfolder}

# The default PHP for our Drupal7.
export VGRTPHPDRUPAL7=${vgrtphpdrupal7}

# The default PHP for Drupal8.
export VGRTPHPDRUPAL8=${vgrtphpdrupal8}

# Required so the escaped text stays escaped in the drupal.sh file.
export VGUWHOST='\$UWhost'
export VGUWPREF='\$UWpref'
export VGUWDB='\$UWdb'
export VGUWUSER='\$UWuser'
export VGUWPASS='\$UWpass'
export VGUWPRI='\$UWpri'
export VGUWCONF='\$conf'
export VGUWCONFD8='\$settings'

# Required for some variables related to the manifest files.
export VGUWSITE='\$site'
export VGUWPROFILE='\$profile'

# The version of PHP we want as the default.
export VGDEFAULTPHPVERSION=${vgrtdefaultphp}
EOF
