#!/bin/bash
## PHPStorm settings

## creating a variable so the file can be referenced correctly.
my_dir="$(dirname "$0")"
. manifests/variable.sh

folders="drupal7 drupal7alt drupal7lrel drupal7rel drupal7os drupal8 drupal8alt drupal8std html"
for folder in $folders
do
 cat > phpstorm/"$folder"/.idea/webServers.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="WebServers">
    <option name="servers">
      <webServer id="da35ba87-1a1a-4be2-9f4c-a1ffec5a6c2d" name="${vgrthostname}" url="https://${vgrthostname}">
        <fileTransfer host="${vgrtip}" port="22" rootFolder="/var/www/" accessType="SFTP" password="dfdcdfcbdfcddfd8dfcbdfc4dfde" username="vagrant">
          <advancedOptions>
            <advancedOptions dataProtectionLevel="Private" />
          </advancedOptions>
        </fileTransfer>
      </webServer>
    </option>
  </component>
</project>
EOF
done

configs="FDSU1 FDSU2 FDSU3 FDSU4 FDSU5"
folders="drupal7 drupal7alt drupal7lrel drupal7rel drupal7os drupal8 drupal8alt html"
sites=5
for config in $configs
do
 for folder in $folders
 do
 servername="${folder/drupal/${vgrtserver}}"
 VGUWCONFIG=$config
 cat > phpstorm/"$folder"/.idea/runConfigurations/"$config".xml <<EOF
  <component name="ProjectRunConfigurationManager">
  <configuration default="false" name="${VGUWCONFIG}" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://$servername/${config/FDSU/fdsu}">
    <method />
  </configuration>
</component>

EOF
 done
done

folders="drupal7 drupal7alt drupal7lrel drupal7rel drupal7os"
for folder in $folders
do
 VGUWFOLDER=$folder
 cat > phpstorm/"$folder"/.idea/deployment.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
 <project version="4">
   <component name="PublishConfigData" autoUpload="Always" serverName="${vgrthostname}">
     <serverData>
       <paths name="${vgrthostname}">
         <serverdata>
           <mappings>
             <mapping deploy="${VGUWFOLDER}" local="\$PROJECT_DIR\$/drupal" web="${VGUWFOLDER}" />
           </mappings>
 	         <excludedPaths>
             <excludedPath path="/${VGUWFOLDER}/fdsu1" />
             <excludedPath path="/${VGUWFOLDER}/fdsu2" />
             <excludedPath path="/${VGUWFOLDER}/fdsu3" />
             <excludedPath path="/${VGUWFOLDER}/fdsu4" />
             <excludedPath path="/${VGUWFOLDER}/fdsu5" />
             <excludedPath path="/${VGUWFOLDER}/includes" />
             <excludedPath path="/${VGUWFOLDER}/misc" />
             <excludedPath path="/${VGUWFOLDER}/scripts" />
             <excludedPath path="/${VGUWFOLDER}/themes" />
             <excludedPath path="/${VGUWFOLDER}/.gitconfig" />
             <excludedPath path="/${VGUWFOLDER}/.htaccess" />
             <excludedPath path="/${VGUWFOLDER}/authorize.php" />
             <excludedPath path="/${VGUWFOLDER}/CHANGELOG.txt" />
             <excludedPath path="/${VGUWFOLDER}/COPYRIGHT.txt" />
             <excludedPath path="/${VGUWFOLDER}/cron.php" />
             <excludedPath path="/${VGUWFOLDER}/.editorconfig" />
             <excludedPath path="/${VGUWFOLDER}/LICENSE.txt" />
             <excludedPath path="/${VGUWFOLDER}/MAINTAINERS.txt" />
             <excludedPath path="/${VGUWFOLDER}/README.txt" />
             <excludedPath path="/${VGUWFOLDER}/fonts" />
             <excludedPath path="/${VGUWFOLDER}/update.php" />
             <excludedPath path="/${VGUWFOLDER}/UPGRADE.txt" />
             <excludedPath path="/${VGUWFOLDER}/web.config" />
             <excludedPath path="/${VGUWFOLDER}/xmlrpc.php" />
 	         </excludedPaths>
           </serverdata>
         </paths>
       </serverData>
       <option name="myAutoUpload" value="ALWAYS" />
     </component>
   </project>
EOF
done
## Seperated D8 because the path is different....
folders="drupal8 drupal8alt drupal8std"
for folder in $folders
do
 VGUWFOLDER=$folder
 cat > phpstorm/"$folder"/.idea/deployment.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
 <project version="4">
   <component name="PublishConfigData" autoUpload="Always" serverName="${vgrthostname}">
     <serverData>
       <paths name="${vgrthostname}">
         <serverdata>
           <mappings>
             <mapping deploy="${VGUWFOLDER}" local="\$PROJECT_DIR\$/drupal" web="${VGUWFOLDER}" />
           </mappings>
 	         <excludedPaths>
             <excludedPath path="/${VGUWFOLDER}/web/fdsu1" />
             <excludedPath path="/${VGUWFOLDER}/web/fdsu2" />
             <excludedPath path="/${VGUWFOLDER}/web/fdsu3" />
             <excludedPath path="/${VGUWFOLDER}/web/fdsu4" />
             <excludedPath path="/${VGUWFOLDER}/web/fdsu5" />
             <excludedPath path="/${VGUWFOLDER}/web/includes" />
             <excludedPath path="/${VGUWFOLDER}/web/misc" />
             <excludedPath path="/${VGUWFOLDER}/web/scripts" />
             <excludedPath path="/${VGUWFOLDER}/web/themes" />
             <excludedPath path="/${VGUWFOLDER}/web/.gitconfig" />
             <excludedPath path="/${VGUWFOLDER}/web/.htaccess" />
             <excludedPath path="/${VGUWFOLDER}/web/authorize.php" />
             <excludedPath path="/${VGUWFOLDER}/web/CHANGELOG.txt" />
             <excludedPath path="/${VGUWFOLDER}/web/COPYRIGHT.txt" />
             <excludedPath path="/${VGUWFOLDER}/web/cron.php" />
             <excludedPath path="/${VGUWFOLDER}/web/.editorconfig" />
             <excludedPath path="/${VGUWFOLDER}/web/LICENSE.txt" />
             <excludedPath path="/${VGUWFOLDER}/web/MAINTAINERS.txt" />
             <excludedPath path="/${VGUWFOLDER}/web/README.txt" />
             <excludedPath path="/${VGUWFOLDER}/web/fonts" />
             <excludedPath path="/${VGUWFOLDER}/web/update.php" />
             <excludedPath path="/${VGUWFOLDER}/web/UPGRADE.txt" />
             <excludedPath path="/${VGUWFOLDER}/web/web.config" />
             <excludedPath path="/${VGUWFOLDER}/web/xmlrpc.php" />
 	         </excludedPaths>
           </serverdata>
         </paths>
       </serverData>
       <option name="myAutoUpload" value="ALWAYS" />
     </component>
   </project>
EOF
done

cat > phpstorm/html/.idea/deployment.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
 <project version="4">
   <component name="PublishConfigData" autoUpload="Always" serverName="${vgrthostname}">
     <serverData>
       <paths name="${vgrthostname}">
         <serverdata>
           <mappings>
             <mapping deploy="html" local="\$PROJECT_DIR\$/drupal" web="html" />
           </mappings>
 	         <excludedPaths>
             <excludedPath path="/html/uw_wcms_pattern_lab/node_modules" />
             <excludedPath path="/html/uw_wcms_pattern_lab/pattern-lab/vendor" />
             <excludedPath path="/html/uw_wcms_pattern_lab/.git" />
 	         </excludedPaths>
           </serverdata>
         </paths>
       </serverData>
       <option name="myAutoUpload" value="ALWAYS" />
     </component>
   </project>
EOF

folders="drupal7 drupal7alt drupal7lrel drupal7rel drupal7os html"
for folder in $folders
do
VGUWFOLDER=$folder
cat > phpstorm/"$folder"/.idea/php.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="Behat">
    <behat_settings>
      <BehatSettings behat_path="" />
    </behat_settings>
  </component>
  <component name="PhpProjectServersManager">
    <servers>
      <server host="${vgrtip}" id="348c7932-4df2-4147-af1b-51e023f14ac8" name="${vgrthostname}" use_path_mappings="true">
        <path_mappings>
          <mapping local-root="\$PROJECT_DIR\$/drupal" remote-root="/var/www/${VGUWFOLDER}" />
        </path_mappings>
      </server>
    </servers>
  </component>
  <component name="PhpProjectSharedConfiguration" php_language_level="5.6.0" />
</project>
EOF
done

folders="drupal8 drupal8alt drupal8std"
for folder in $folders
do
VGUWFOLDER=$folder
cat > phpstorm/"$folder"/.idea/php.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="Behat">
    <behat_settings>
      <BehatSettings behat_path="" />
    </behat_settings>
  </component>
  <component name="PhpProjectServersManager">
    <servers>
      <server host="${vgrtip}" id="348c7932-4df2-4147-af1b-51e023f14ac8" name="${vgrthostname}" use_path_mappings="true">
        <path_mappings>
          <mapping local-root="\$PROJECT_DIR\$/drupal" remote-root="/var/www/${VGUWFOLDER}/web" />
        </path_mappings>
      </server>
    </servers>
  </component>
  <component name="PhpProjectSharedConfiguration" php_language_level="7.4.0" />
</project>
EOF
done

folders="drupal7 drupal7alt drupal7lrel drupal7rel drupal7os html"
for folder in $folders
do
VGUWFOLDER=$folder
cat > phpstorm/"$folder"/.idea/workspace.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="ChangeListManager">
    <list default="true" id="080b2ab3-b6a9-48a3-a0ad-58290f2bc748" name="Default" comment="" />
    <ignored path="WCMS-vagrant.iws" />
    <ignored path=".idea/workspace.xml" />
    <ignored path=".idea/dataSources.local.xml" />
    <option name="EXCLUDED_CONVERTED_TO_IGNORED" value="true" />
    <option name="TRACKING_ENABLED" value="true" />
    <option name="SHOW_DIALOG" value="false" />
    <option name="HIGHLIGHT_CONFLICTS" value="true" />
    <option name="HIGHLIGHT_NON_ACTIVE_CHANGELIST" value="false" />
    <option name="LAST_RESOLUTION" value="IGNORE" />
  </component>
  <component name="ChangesViewManager" flattened_view="true" show_ignored="false" />
  <component name="CreatePatchCommitExecutor">
    <option name="PATCH_PATH" value="" />
  </component>
  <component name="ExecutionTargetManager" SELECTED_TARGET="default_target" />
  <component name="FavoritesManager">
    <favorites_list name="${vgrthostname}" />
  </component>
  <component name="FileEditorManager">
    <leaf />
  </component>
  <component name="Git.Settings">
    <option name="RECENT_GIT_ROOT_PATH" value="\$PROJECT_DIR\$/.." />
  </component>
  <component name="JsBuildToolGruntFileManager" detection-done="true" />
  <component name="JsBuildToolPackageJson" detection-done="true" />
  <component name="JsGulpfileManager">
    <detection-done>true</detection-done>
  </component>
  <component name="PhpServers">
    <servers>
      <server host="${vgrtip}" id="9cf2afed-5098-4346-9ada-c8e87401ea00" name="${vgrthostname}" use_path_mappings="true">
        <path_mappings>
          <mapping local-root="\$PROJECT_DIR\$/drupal" remote-root="/var/www/${VGUWFOLDER}" />
        </path_mappings>
      </server>
    </servers>
    <include_path>
      <path value="/usr/share/php" />
    </include_path>
  </component>
  <component name="ProjectFrameBounds">
    <option name="x" value="-1928" />
    <option name="y" value="-329" />
    <option name="width" value="1936" />
    <option name="height" value="1056" />
  </component>
  <component name="ProjectLevelVcsManager" settingsEditedManually="true">
    <OptionsSetting value="true" id="Add" />
    <OptionsSetting value="true" id="Remove" />
    <OptionsSetting value="true" id="Checkout" />
    <OptionsSetting value="true" id="Update" />
    <OptionsSetting value="true" id="Status" />
    <OptionsSetting value="true" id="Edit" />
    <ConfirmationsSetting value="0" id="Add" />
    <ConfirmationsSetting value="0" id="Remove" />
  </component>
  <component name="ProjectView">
    <navigator currentView="ProjectPane" proportions="" version="1">
      <flattenPackages />
      <showMembers />
      <showModules />
      <showLibraryContents />
      <hideEmptyPackages />
      <abbreviatePackageNames />
      <autoscrollToSource />
      <autoscrollFromSource />
      <sortByType />
      <manualOrder />
      <foldersAlwaysOnTop value="true" />
    </navigator>
    <panes>
      <pane id="Scratches" />
      <pane id="ProjectPane">
        <subPane>
        <PATH>
          <PATH_ELEMENT>
            <option name="myItemId" value="${vgrthostname}" />
            <option name="myItemType" value="com.intellij.ide.projectView.impl.nodes.ProjectViewProjectNode" />
          </PATH_ELEMENT>
        </PATH>
          <PATH>
            <PATH_ELEMENT>
              <option name="myItemId" value="${vgrthostname}" />
              <option name="myItemType" value="com.intellij.ide.projectView.impl.nodes.ProjectViewProjectNode" />
            </PATH_ELEMENT>
            <PATH_ELEMENT>
              <option name="myItemId" value="${VGUWFOLDER}" />
              <option name="myItemType" value="com.intellij.ide.projectView.impl.nodes.PsiDirectoryNode" />
            </PATH_ELEMENT>
          </PATH>
        </subPane>
      </pane>
      <pane id="Scope">
        <subPane subId="Project Files">
          <PATH>
            <PATH_ELEMENT USER_OBJECT="Root">
              <option name="myItemId" value="" />
              <option name="myItemType" value="" />
            </PATH_ELEMENT>
          </PATH>
        </subPane>
      </pane>
    </panes>
  </component>
  <component name="PropertiesComponent">
    <property name="options.lastSelected" value="reference.settingsdialog.project.vagrant" />
    <property name="options.splitter.main.proportions" value="0.3" />
    <property name="options.splitter.details.proportions" value="0.2" />
    <property name="WebServerToolWindowFactoryState" value="true" />
    <property name="last_opened_file_path" value="\$PROJECT_DIR\$/www" />
    <property name="settings.editor.selected.configurable" value="reference.webide.settings.project.settings.php.behat" />
    <property name="settings.editor.splitter.proportion" value="0.2" />
  </component>
  <component name="RunManager" selected="PHP Web Application.HTML">
    <configuration default="true" type="JavascriptDebugType" factoryName="JavaScript Debug">
      <method />
    </configuration>
    <configuration default="true" type="PHPUnitRunConfigurationType" factoryName="PHPUnit">
      <TestRunner />
      <method />
    </configuration>
    <configuration default="true" type="PhpBehatConfigurationType" factoryName="Behat">
      <BehatRunner />
      <method />
    </configuration>
    <configuration default="true" type="PhpLocalRunConfigurationType" factoryName="PHP Console">
      <method />
    </configuration>
    <configuration default="true" type="PhpRemoteDebugRunConfigurationType" factoryName="PHP Remote Debug" server_name="${vgrthostname}">
      <method />
    </configuration>
    <configuration default="true" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" server_name="${vgrthostname}" start_url="https://${vgrtserver}7.${VGSITE}1">
      <method />
    </configuration>
    <configuration default="true" type="js.build_tools.gulp" factoryName="Gulp.js">
      <node-options />
      <gulpfile />
      <tasks />
      <arguments />
      <envs />
      <method />
    </configuration>
    <configuration default="true" type="js.build_tools.npm" factoryName="npm">
      <command value="run-script" />
      <scripts />
      <envs />
      <method />
    </configuration>
    <configuration default="false" name="FDSU1" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}7.${VGSITE}1">
      <method />
    </configuration>
    <configuration default="false" name="FDSU2" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}7.${VGSITE}2">
      <method />
    </configuration>
    <configuration default="false" name="FDSU3" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}7.${VGSITE}3">
      <method />
    </configuration>
    <configuration default="false" name="FDSU4" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}7.${VGSITE}4">
      <method />
    </configuration>
    <configuration default="false" name="FDSU5" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}7.${VGSITE}5">
      <method />
    </configuration>
    <list size="5">
      <item index="0" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}1" />
      <item index="1" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}2" />
      <item index="2" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}3" />
      <item index="3" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}4" />
      <item index="4" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}5" />
    </list>
  </component>
  <component name="ShelveChangesManager" show_recycled="false" />
  <component name="SvnConfiguration">
    <configuration />
  </component>
  <component name="TaskManager">
    <task active="true" id="Default" summary="Default task">
      <changelist id="080b2ab3-b6a9-48a3-a0ad-58290f2bc748" name="Default" comment="" />
      <created>1449598595296</created>
      <option name="number" value="Default" />
      <updated>1449598595296</updated>
    </task>
    <servers />
  </component>
  <component name="ToolWindowManager">
    <frame x="-1928" y="-329" width="1936" height="1056" extended-state="6" />
    <editor active="false" />
    <layout>
      <window_info id="Remote Host" active="false" anchor="right" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="4" side_tool="false" content_ui="tabs" />
      <window_info id="Project" active="true" anchor="left" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="true" show_stripe_button="true" weight="0.15884861" sideWeight="0.5" order="0" side_tool="false" content_ui="tabs" />
      <window_info id="TODO" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.3299611" sideWeight="0.5" order="6" side_tool="false" content_ui="tabs" />
      <window_info id="Event Log" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.22352025" sideWeight="0.5" order="8" side_tool="true" content_ui="tabs" />
      <window_info id="Database" active="false" anchor="right" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="3" side_tool="false" content_ui="tabs" />
      <window_info id="Version Control" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.3299611" sideWeight="0.5" order="10" side_tool="false" content_ui="tabs" />
      <window_info id="Run" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="2" side_tool="false" content_ui="tabs" />
      <window_info id="Structure" active="false" anchor="left" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.25" sideWeight="0.5" order="1" side_tool="false" content_ui="tabs" />
      <window_info id="Terminal" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="11" side_tool="false" content_ui="tabs" />
      <window_info id="Favorites" active="false" anchor="left" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="2" side_tool="true" content_ui="tabs" />
      <window_info id="Debug" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.38786566" sideWeight="0.5" order="3" side_tool="false" content_ui="tabs" />
      <window_info id="Changes" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.061912227" sideWeight="0.5" order="7" side_tool="false" content_ui="tabs" />
      <window_info id="Cvs" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.25" sideWeight="0.5" order="4" side_tool="false" content_ui="tabs" />
      <window_info id="Message" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="0" side_tool="false" content_ui="tabs" />
      <window_info id="Commander" active="false" anchor="right" auto_hide="false" internal_type="SLIDING" type="SLIDING" visible="false" show_stripe_button="true" weight="0.4" sideWeight="0.5" order="0" side_tool="false" content_ui="tabs" />
      <window_info id="Application Servers" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="9" side_tool="false" content_ui="tabs" />
      <window_info id="Inspection" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.4" sideWeight="0.5" order="5" side_tool="false" content_ui="tabs" />
      <window_info id="File Transfer" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.32900432" sideWeight="0.5" order="12" side_tool="false" content_ui="tabs" />
      <window_info id="Hierarchy" active="false" anchor="right" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.25" sideWeight="0.5" order="2" side_tool="false" content_ui="combo" />
      <window_info id="Find" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.2225681" sideWeight="0.5" order="1" side_tool="false" content_ui="tabs" />
      <window_info id="Ant Build" active="false" anchor="right" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.25" sideWeight="0.5" order="1" side_tool="false" content_ui="tabs" />
    </layout>
  </component>
  <component name="VcsContentAnnotationSettings">
    <option name="myLimit" value="2678400000" />
  </component>
  <component name="XDebuggerManager">
    <breakpoint-manager>
      <option name="time" value="3" />
    </breakpoint-manager>
    <watches-manager />
  </component>
</project>
EOF
done

folders="drupal8 drupal8alt drupal8std"
for folder in $folders
do
VGUWFOLDER=$folder
cat > phpstorm/"$folder"/.idea/workspace.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="ChangeListManager">
    <list default="true" id="080b2ab3-b6a9-48a3-a0ad-58290f2bc748" name="Default" comment="" />
    <ignored path="WCMS-vagrant.iws" />
    <ignored path=".idea/workspace.xml" />
    <ignored path=".idea/dataSources.local.xml" />
    <option name="EXCLUDED_CONVERTED_TO_IGNORED" value="true" />
    <option name="TRACKING_ENABLED" value="true" />
    <option name="SHOW_DIALOG" value="false" />
    <option name="HIGHLIGHT_CONFLICTS" value="true" />
    <option name="HIGHLIGHT_NON_ACTIVE_CHANGELIST" value="false" />
    <option name="LAST_RESOLUTION" value="IGNORE" />
  </component>
  <component name="ChangesViewManager" flattened_view="true" show_ignored="false" />
  <component name="CreatePatchCommitExecutor">
    <option name="PATCH_PATH" value="" />
  </component>
  <component name="ExecutionTargetManager" SELECTED_TARGET="default_target" />
  <component name="FavoritesManager">
    <favorites_list name="${vgrthostname}" />
  </component>
  <component name="FileEditorManager">
    <leaf />
  </component>
  <component name="Git.Settings">
    <option name="RECENT_GIT_ROOT_PATH" value="\$PROJECT_DIR\$/.." />
  </component>
  <component name="JsBuildToolGruntFileManager" detection-done="true" />
  <component name="JsBuildToolPackageJson" detection-done="true" />
  <component name="JsGulpfileManager">
    <detection-done>true</detection-done>
  </component>
  <component name="PhpServers">
    <servers>
      <server host="${vgrtip}" id="9cf2afed-5098-4346-9ada-c8e87401ea00" name="${vgrthostname}" use_path_mappings="true">
        <path_mappings>
          <mapping local-root="\$PROJECT_DIR\$/drupal" remote-root="/var/www/${VGUWFOLDER}/web" />
        </path_mappings>
      </server>
    </servers>
    <include_path>
      <path value="/usr/share/php" />
    </include_path>
  </component>
  <component name="ProjectFrameBounds">
    <option name="x" value="-1928" />
    <option name="y" value="-329" />
    <option name="width" value="1936" />
    <option name="height" value="1056" />
  </component>
  <component name="ProjectLevelVcsManager" settingsEditedManually="true">
    <OptionsSetting value="true" id="Add" />
    <OptionsSetting value="true" id="Remove" />
    <OptionsSetting value="true" id="Checkout" />
    <OptionsSetting value="true" id="Update" />
    <OptionsSetting value="true" id="Status" />
    <OptionsSetting value="true" id="Edit" />
    <ConfirmationsSetting value="0" id="Add" />
    <ConfirmationsSetting value="0" id="Remove" />
  </component>
  <component name="ProjectView">
    <navigator currentView="ProjectPane" proportions="" version="1">
      <flattenPackages />
      <showMembers />
      <showModules />
      <showLibraryContents />
      <hideEmptyPackages />
      <abbreviatePackageNames />
      <autoscrollToSource />
      <autoscrollFromSource />
      <sortByType />
      <manualOrder />
      <foldersAlwaysOnTop value="true" />
    </navigator>
    <panes>
      <pane id="Scratches" />
      <pane id="ProjectPane">
        <subPane>
        <PATH>
          <PATH_ELEMENT>
            <option name="myItemId" value="${vgrthostname}" />
            <option name="myItemType" value="com.intellij.ide.projectView.impl.nodes.ProjectViewProjectNode" />
          </PATH_ELEMENT>
        </PATH>
          <PATH>
            <PATH_ELEMENT>
              <option name="myItemId" value="${vgrthostname}" />
              <option name="myItemType" value="com.intellij.ide.projectView.impl.nodes.ProjectViewProjectNode" />
            </PATH_ELEMENT>
            <PATH_ELEMENT>
              <option name="myItemId" value="${VGUWFOLDER}" />
              <option name="myItemType" value="com.intellij.ide.projectView.impl.nodes.PsiDirectoryNode" />
            </PATH_ELEMENT>
          </PATH>
        </subPane>
      </pane>
      <pane id="Scope">
        <subPane subId="Project Files">
          <PATH>
            <PATH_ELEMENT USER_OBJECT="Root">
              <option name="myItemId" value="" />
              <option name="myItemType" value="" />
            </PATH_ELEMENT>
          </PATH>
        </subPane>
      </pane>
    </panes>
  </component>
  <component name="PropertiesComponent">
    <property name="options.lastSelected" value="reference.settingsdialog.project.vagrant" />
    <property name="options.splitter.main.proportions" value="0.3" />
    <property name="options.splitter.details.proportions" value="0.2" />
    <property name="WebServerToolWindowFactoryState" value="true" />
    <property name="last_opened_file_path" value="\$PROJECT_DIR\$/www" />
    <property name="settings.editor.selected.configurable" value="reference.webide.settings.project.settings.php.behat" />
    <property name="settings.editor.splitter.proportion" value="0.2" />
  </component>
  <component name="RunManager" selected="PHP Web Application.HTML">
    <configuration default="true" type="JavascriptDebugType" factoryName="JavaScript Debug">
      <method />
    </configuration>
    <configuration default="true" type="PHPUnitRunConfigurationType" factoryName="PHPUnit">
      <TestRunner />
      <method />
    </configuration>
    <configuration default="true" type="PhpBehatConfigurationType" factoryName="Behat">
      <BehatRunner />
      <method />
    </configuration>
    <configuration default="true" type="PhpLocalRunConfigurationType" factoryName="PHP Console">
      <method />
    </configuration>
    <configuration default="true" type="PhpRemoteDebugRunConfigurationType" factoryName="PHP Remote Debug" server_name="${vgrthostname}">
      <method />
    </configuration>
    <configuration default="true" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" server_name="${vgrthostname}" start_url="https://${vgrtserver}8.${VGSITE}1">
      <method />
    </configuration>
    <configuration default="true" type="js.build_tools.gulp" factoryName="Gulp.js">
      <node-options />
      <gulpfile />
      <tasks />
      <arguments />
      <envs />
      <method />
    </configuration>
    <configuration default="true" type="js.build_tools.npm" factoryName="npm">
      <command value="run-script" />
      <scripts />
      <envs />
      <method />
    </configuration>
    <configuration default="false" name="FDSU1" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}8.${VGSITE}1">
      <method />
    </configuration>
    <configuration default="false" name="FDSU2" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}8.${VGSITE}2">
      <method />
    </configuration>
    <configuration default="false" name="FDSU3" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}8.${VGSITE}3">
      <method />
    </configuration>
    <configuration default="false" name="FDSU4" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}8.${VGSITE}4">
      <method />
    </configuration>
    <configuration default="false" name="FDSU5" type="PhpWebAppRunConfigurationType" factoryName="PHP Web Application" browser="98ca6316-2f89-46d9-a9e5-fa9e2b0625b3" server_name="${vgrthostname}" start_url="https://${vgrtserver}8.${VGSITE}5">
      <method />
    </configuration>
    <list size="5">
      <item index="0" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}1" />
      <item index="1" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}2" />
      <item index="2" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}3" />
      <item index="3" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}4" />
      <item index="4" class="java.lang.String" itemvalue="PHP Web Application.${VGSITE}5" />
    </list>
  </component>
  <component name="ShelveChangesManager" show_recycled="false" />
  <component name="SvnConfiguration">
    <configuration />
  </component>
  <component name="TaskManager">
    <task active="true" id="Default" summary="Default task">
      <changelist id="080b2ab3-b6a9-48a3-a0ad-58290f2bc748" name="Default" comment="" />
      <created>1449598595296</created>
      <option name="number" value="Default" />
      <updated>1449598595296</updated>
    </task>
    <servers />
  </component>
  <component name="ToolWindowManager">
    <frame x="-1928" y="-329" width="1936" height="1056" extended-state="6" />
    <editor active="false" />
    <layout>
      <window_info id="Remote Host" active="false" anchor="right" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="4" side_tool="false" content_ui="tabs" />
      <window_info id="Project" active="true" anchor="left" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="true" show_stripe_button="true" weight="0.15884861" sideWeight="0.5" order="0" side_tool="false" content_ui="tabs" />
      <window_info id="TODO" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.3299611" sideWeight="0.5" order="6" side_tool="false" content_ui="tabs" />
      <window_info id="Event Log" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.22352025" sideWeight="0.5" order="8" side_tool="true" content_ui="tabs" />
      <window_info id="Database" active="false" anchor="right" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="3" side_tool="false" content_ui="tabs" />
      <window_info id="Version Control" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.3299611" sideWeight="0.5" order="10" side_tool="false" content_ui="tabs" />
      <window_info id="Run" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="2" side_tool="false" content_ui="tabs" />
      <window_info id="Structure" active="false" anchor="left" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.25" sideWeight="0.5" order="1" side_tool="false" content_ui="tabs" />
      <window_info id="Terminal" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="11" side_tool="false" content_ui="tabs" />
      <window_info id="Favorites" active="false" anchor="left" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="2" side_tool="true" content_ui="tabs" />
      <window_info id="Debug" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.38786566" sideWeight="0.5" order="3" side_tool="false" content_ui="tabs" />
      <window_info id="Changes" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.061912227" sideWeight="0.5" order="7" side_tool="false" content_ui="tabs" />
      <window_info id="Cvs" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.25" sideWeight="0.5" order="4" side_tool="false" content_ui="tabs" />
      <window_info id="Message" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="0" side_tool="false" content_ui="tabs" />
      <window_info id="Commander" active="false" anchor="right" auto_hide="false" internal_type="SLIDING" type="SLIDING" visible="false" show_stripe_button="true" weight="0.4" sideWeight="0.5" order="0" side_tool="false" content_ui="tabs" />
      <window_info id="Application Servers" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.33" sideWeight="0.5" order="9" side_tool="false" content_ui="tabs" />
      <window_info id="Inspection" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.4" sideWeight="0.5" order="5" side_tool="false" content_ui="tabs" />
      <window_info id="File Transfer" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.32900432" sideWeight="0.5" order="12" side_tool="false" content_ui="tabs" />
      <window_info id="Hierarchy" active="false" anchor="right" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.25" sideWeight="0.5" order="2" side_tool="false" content_ui="combo" />
      <window_info id="Find" active="false" anchor="bottom" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.2225681" sideWeight="0.5" order="1" side_tool="false" content_ui="tabs" />
      <window_info id="Ant Build" active="false" anchor="right" auto_hide="false" internal_type="DOCKED" type="DOCKED" visible="false" show_stripe_button="true" weight="0.25" sideWeight="0.5" order="1" side_tool="false" content_ui="tabs" />
    </layout>
  </component>
  <component name="VcsContentAnnotationSettings">
    <option name="myLimit" value="2678400000" />
  </component>
  <component name="XDebuggerManager">
    <breakpoint-manager>
      <option name="time" value="3" />
    </breakpoint-manager>
    <watches-manager />
  </component>
</project>
EOF
done

folders="drupal7 drupal7alt drupal7lrel drupal7rel drupal7os drupal8 drupal8alt html"
for folder in $folders
do
cat > phpstorm/"$folder"/.idea/.name <<EOF
${vgrthostname}
EOF
done

folders="drupal7 drupal7alt drupal7lrel drupal7rel drupal7os drupal8 drupal8alt html"
for folder in $folders
do
cat > phpstorm/"$folder"/.idea/inspectionProfiles/Project_Default.xml <<EOF
<component name="InspectionProjectProfileManager">
  <profile version="1.0">
    <option name="myName" value="Project Default" />
    <inspection_tool class="MessDetectorValidationInspection" enabled="true" level="WARNING" enabled_by_default="true">
      <option name="CODESIZE" value="true" />
      <option name="CONTROVERSIAL" value="true" />
      <option name="DESIGN" value="true" />
      <option name="UNUSEDCODE" value="true" />
      <option name="NAMING" value="true" />
    </inspection_tool>
    <inspection_tool class="PhpCSValidationInspection" enabled="true" level="WARNING" enabled_by_default="true" />
  </profile>
</component>
EOF
done
