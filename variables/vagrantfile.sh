#!/bin/bash

echo ""
echo "Writing Vagrantfile"
echo ""

cat > Vagrantfile <<EOF
Vagrant.configure("2") do |config|
  config.vm.box = "bento/ubuntu-18.04"
  config.vm.hostname = "${vgrthostname}"
  config.vm.network "private_network", ip: "${vgrtip}", gateway: "192.168.101.1", type: "static"
  ## Use NFS for shared folders for better performance
  ##config.vm.synced_folder '.', '/vagrant', nfs: true, nfs_export: false
  ## To fix a potential issue with Windows 10
  #config.vm.synced_folder ".", "/vagrant", type: "nfs", nfs_version: 3
  config.vm.provider "vmware_desktop" do |vmware|
    vmware.ssh_info_public = true
    vmware.vmx["displayName"] = "${vgrthostname}"
    vmware.vmx["memsize"] = "4096"
    vmware.vmx["numvcpus"] = "2"
    ## Added because of this warning:
    ## The VMX file for this box contains a setting that is automatically overwritten by Vagrant when started.
    ## Vagrant will stop overwriting this setting in an upcoming release which may prevent proper networking setup.
    vmware.vmx["ethernet0.pcislotnumber"] = "32"
    vmware.vmx["ethernet1.pcislotnumber"] = "34"
    ## Added so the vagrant machine shows up in VMWare (like it did in Virtualbox),
    ## NOTE: it doesn't delete from VMWare when the vagrant machine is destroyed
    ## Commented out but left it in so people have the option to turn it on.
    ## vmware.gui = true
  end
  ## These scripts will be executed on the VM, to install things like Apache & PHP

  ## Remove the stdin is not tty error during install
    config.vm.provision :shell,
    :inline => "sed -i 's/^mesg n$/tty -s \\&\\& mesg n/g' /root/.profile"

  ## Add the variable script so we can access the variables while building the VM.
    config.vm.provision "file", source: "manifests/variable.sh", destination: "/tmp/variable.sh"

  ## Add the WCMS.crt file to the VM so the composer install command can access wms-ci-s1.
    config.vm.provision "file", source: "variables/WCMS.crt", destination: "/tmp/WCMS.crt"

  ## Add server.crt and key to the server so we don't keep creating new ones.
    config.vm.provision "file", source: "variables/server.crt", destination: "/tmp/server.crt"
    config.vm.provision "file", source: "variables/server.key", destination: "/tmp/server.key"
    config.vm.provision "shell", inline: "mv /tmp/server.crt /etc/ssl/certs/server.crt"
    config.vm.provision "shell", inline: "mv /tmp/server.key /etc/ssl/private/server.key"

  ## Provision script - run once
    config.vm.provision "shell", path: "manifests/ubuntu.sh"

  ## Copy the vhost file to default and reload apache - run every vagrant up
    config.vm.provision "shell", path: "manifests/apache.sh"

  ## Copy the drupal settings - run every vagrant up
    config.vm.provision "shell", path: "manifests/drupal7.sh"
    config.vm.provision "shell", path: "manifests/drupal8.sh"
    config.vm.provision "shell", path: "manifests/drupal-general.sh"

  ## Add the settings.local and development.services.yml files to the build so we can copy them in where needed.
    config.vm.provision "file", source: "variables/settings.local.php", destination: "/tmp/settings.local.php"
    config.vm.provision "file", source: "variables/development.services.yml", destination: "/tmp/development.services.yml"
    config.vm.provision "shell", inline: "mv /tmp/settings.local.php /var/www/drupal8/web/sites/${vgrtserver}8.fdsu1/settings.local.php"
    config.vm.provision "shell", inline: "mv /tmp/development.services.yml /var/www/drupal8/web/sites/${vgrtserver}8.fdsu1/development.services.yml"
    config.vm.provision "file", source: "variables/wcms-dump.sql", destination: "/tmp/wcms-dump.sql"
    config.vm.provision "shell", inline: "mv /tmp/wcms-dump.sql /var/www/drupal8/web/sites/${vgrtserver}8.fdsu1/wcms-dump.sql"

  ## Copy the fonts over if they exist:
  ##config.vm.provision "file", source: "${vgrtfontfolder}", destination: "/var/www/drupal7/fonts"

  ## Copy the alias files and install phpmyadmin (basically, finish installing all the things...) - run every vagrant up
    config.vm.provision "shell", path: "manifests/finish.sh"
end
EOF
