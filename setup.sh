#!/bin/bash

## Vagrantfile variables
VGHOSTNAME='wcms-vagrant'
VGIP='192.168.101.20'
VGSITE='fdsu'
VGSERVER='d'
VGPHPDRUPAL7='7.3'
VGPHPDRUPAL8='7.4'

## Check to make sure we will not clobber an existing vm config.
if [ -e Vagrantfile ]
  then
    echo "Vagrantfile exists"
    echo "If you want a new environment first run vagrant destroy to ensure everything is cleaned up."
    echo ""
    echo "Then run ./clean.sh to clean everything out."
    echo ""
    echo "** THE ABOVE STEPS WILL DELETE EVERYTHING SO MAKE SURE ALL WORK IS COMMITTED AND PUSHED **"
    exit
fi
echo "
********************************************************************************
First we will ask you a few questions to identify this host. Defaults are enclosed in single square
brackets, pressing enter will choose the default setting.
********************************************************************************
"
read -rp "hostname [${VGHOSTNAME}]: " vgrthostname
export vgrthostname=${vgrthostname:-$VGHOSTNAME}
echo ""
## set the IP address for the Virtual Machine it has to start 192.168.101.xx
unset vgrtip
while [[ ! ${vgrtip} =~ ^192\.168\.101\.[0-9]{1,3}$ ]]; do
read -rp "IP address (it has to start 192.168.101.xx) [${VGIP}]: " vgrtip
vgrtip=${vgrtip:-$VGIP}
  if [[ ! ${vgrtip} =~ ^192\.168\.101\.[0-9]{1,3}$ ]] ; then
    echo 'You entered' "${vgrtip}" 'The IP address has to start with 192.168.101.xx'
  else
    echo "You entered a valid IP address."
    export vgrtip=${vgrtip:-$VGIP}
  fi
done

echo ""
## read -p "site preface (make sure it doesn't contain . or $ or *) [$VGSITE]": vgrtsite
## vgrtsite=${vgrtsite:-$VGSITE}
export vgrtsite=$VGSITE
read -rp "Set what you want the URL to be (https://<server>7/) for ther servers. Such as d or www (make sure it doesn't contain . or $ or *) [$VGSERVER]: " vgrtserver
export vgrtserver=${vgrtserver:-$VGSERVER}
echo ""
unset vgrtdefaultphp
while [[ ! ${vgrtdefaultphp} ]]; do
echo "You must decide which version of PHP you want to use. Drupal 7 requires $VGPHPDRUPAL7 (to match our servers) and Drupal 8 uses $VGPHPDRUPAL8."
read -rp "which version of php are you going to use? 1) $VGPHPDRUPAL7 2) $VGPHPDRUPAL8: " vgrtdefaultphp
  case $vgrtdefaultphp in
    1) echo "You chose $VGPHPDRUPAL7"
       export vgrtdefaultphp=$VGPHPDRUPAL7;;
       2) echo "You chose $VGPHPDRUPAL8"
      export vgrtdefaultphp=$VGPHPDRUPAL8;;
      *) echo "Sorry, invalid entry";;
  esac
done
## break
echo ""
export vgrtphpdrupal7=$VGPHPDRUPAL7
export vgrtphpdrupal8=$VGPHPDRUPAL8
echo ""
unset vgrtwatiamid
while [[ ! ${vgrtwatiamid} =~ ^[a-z0-9_-]{1,8}$ ]]; do
  read -rp "Please enter your WATIAM ID so it can be set in the .gitconfig: " vgrtwatiamid
  export vgrtwatiamid
done
echo ""
VGEMAIL=${vgrtwatiamid}'@uwaterloo.ca'
unset vgrtemail
while [[ ! ${vgrtemail} =~ ^[a-z0-9]+(\.[a-z0-9]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+([a-z0-9-]*[a-z0-9])$ ]]; do
  read -rp "Please enter your uwaterloo email address so it can be set in the .gitconfig. [${VGEMAIL}]: " vgrtemail
  export vgrtemail=${vgrtemail:-$VGEMAIL}
done

echo 'Create the Variable file'
. ./variables/var.sh
chmod a+x ./manifests/variable.sh
echo ''
echo 'Variable file created.'
echo ''
echo 'Create the Vagrant file.'
. ./variables/vagrantfile.sh
echo 'Vagrant file created.'
echo ''
echo 'Create PHPStorm setting files.'
cd "$(dirname "$0")" || exit
. ./variables/phpstorm.sh
echo ''
echo "
********************************************************************************
You should now be ready to initialize your vagrant instance

This is done via

      vagrant up

The default user is vagrant and the default password is vagrant.

********************************************************************************
"
## $SHELL
