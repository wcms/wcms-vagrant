#!/bin/bash

if [ -e .vagrantfile ]
  then
    echo ".vagrantfile still exists. "
    echo "First run vagrant destroy if you want to create a new environment."
    exit
fi
if [ -e Vagrantfile ]
  then
    echo "Removing Vagrantfile, variable.sh and PHPStorm files."
    rm Vagrantfile
    if [ -e vagrant.log ]
      then
        rm vagrant.log
    fi
    rm manifests/variable.sh
    profiles="drupal7 drupal7alt drupal7rel drupal7os html"
    for profile in $profiles
    do
    rm phpstorm/"$profile"/.idea/deployment.xml
    rm phpstorm/"$profile"/.idea/webServers.xml
    rm phpstorm/"$profile"/.idea/php.xml
    rm phpstorm/"$profile"/.idea/workspace.xml
    rm phpstorm/"$profile"/.idea/.name
    rm phpstorm/"$profile"/.idea/inspectionProfiles/Project_Default.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU1.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU2.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU3.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU4.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU5.xml
    rm -rf phpstorm/"$profile"/drupal/*
    done

    profiles="drupal8 drupal8alt"
    for profile in $profiles
    do
    rm phpstorm/"$profile"/.idea/deployment.xml
    rm phpstorm/"$profile"/.idea/webServers.xml
    rm phpstorm/"$profile"/.idea/php.xml
    rm phpstorm/"$profile"/.idea/workspace.xml
    rm phpstorm/"$profile"/.idea/.name
    rm phpstorm/"$profile"/.idea/inspectionProfiles/Project_Default.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU1.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU2.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU3.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU4.xml
    rm phpstorm/"$profile"/.idea/runConfigurations/FDSU5.xml
    rm -rf phpstorm/"$profile"/drupal/*
    if [ -e .csslintrc ]
      then
        rm phpstorm/"$profile"/drupal/.csslintrc
        rm phpstorm/"$profile"/drupal/.eslintignore
        rm phpstorm/"$profile"/drupal/.eslintrc.json
        rm phpstorm/"$profile"/drupal/.gitattributes
    fi
    done

    echo "All relevant files have been deleted."
 else
   echo "Vagrantfile doesn't exist so no files where deleted."
fi
