# WCMS Vagrantfile setup


**CAUTION!** This Vagrant box is meant for local development only. This set up is not hardened and is not secure, so only use it as a local development environment.

**CAUTION!** This branch is currently in active development and will be changing frequently. Any known issues will be recorded here.

**Note** We are using Ubuntu 18.04 LTS as the base box, currently (Feb.2021), I don't feel while we are still supporting Drupal 7 that this will change. Ubuntu 20.04 LTS installs PHP8 as the default, and too many of the pieces we need don't work "out of the box" making a move non-trivial.

## Prerequisites:

You will have to have local admin rights to install everything. If you have a managed Windows machine, you may not have these by default.

On Windows machines, you will need to have Hyper-V disabled under Windows Features and also within the BIOS. If you only disable under the features and restart your machine could blue screen.

You need to have [Git](https://git-scm.com/downloads), [Vagrant](https://www.vagrantup.com/downloads.html) and [VMWare Workstation Pro (PC)](https://my.vmware.com/en/web/vmware/info/slug/desktop_end_user_computing/vmware_workstation_pro/15_0) or [VMWare Fusion Pro (OSX)](https://my.vmware.com/en/web/vmware/info/slug/desktop_end_user_computing/vmware_fusion/11_0) installed on your host system to create the dev environment. If you are on a Mac, you will also need Xcode installed.

You will need to download and install the [vagrant-vmware-utility](https://www.vagrantup.com/vmware/downloads.html) for your OS. You will need to add the license file (which you would get when you purchased the vagrant-vmware plugin). The license email includes instructions for installing.

## Known issues with 4.x/5.x:

 - Windows - during install the private network adapter doesn't appear to respect the setting from the Vagrantfile, if you can't log in, check the adapter to make sure the correct IP is being used (192.168.101.0). If not, you will need to delete the adapter and re-create it using the correct IP.
 - Mac - if VMWare is not running, you could get a connection error saying the port is unavailable. Opening VMWare before running `vagrant up` appears to fix the issue.

## Getting started:

### Running the initial setup script

Check out this repo anywhere on your system, then run **setup.sh** to begin setup.

Running **setup.sh** creates a Vagrantfile that will allow you to create a virtual machine that runs Ubuntu 18.04 LTS with Apache 2.4, MySQL 5.7 and PHP 7.3/7.4 as well as Drupal currently at 7.78/8.9.
Please note that out of the box this setup only allows local logins to sites. You will need to add an additional file if you require CAS, which you will need to contact a WCMS team member to get.

When you run **setup.sh** it will ask you for a hostname, an IP address (which needs to start with 192.168.101), what your server will be e.g. d7 or k7 etc. You can leave the defaults if you want but if you are running or plan to run multiple WCMS virtual machines then you should change some of the defaults for each one. It will also ask you to choose a version of PHP and for your WATIAM ID so it can preconfigure things within the VM.

**NOTE** Before running **setup.sh** Be aware, you may need to run the vagrant commands as an administrator or at least supply an administrator password for your local dev site to build correctly.

### Vagrant configuration

1. Windows users need to add the winnfsd plugin:

   ```sh
   $ vagrant plugin install vagrant-winnfsd
   ```

2. If you just downloaded and installed vagrant (and wcms-vagrant repo) you could skip this step, otherwise before running you should make sure that the base box is updated to the most recent version:

   ```sh
   $ vagrant box update
   ```

3. If you get an error running the previous step, you may also need to update the Vagrant VMWare Utility which can be found here:

https://www.vagrantup.com/vmware/downloads


4. You need to add the IP address and server name you created during setup to the host machine's "hosts" file. This **may** require admin access.
   On a Windows machine, the hosts file is in C:\Windows\System32\drivers\etc.
   On a Mac, the hosts file is in /etc.

   Your hosts file should look something like this:

   ```sh
   192.168.101.20 wcms-vagrant d7 d7alt d7rel d7lrel d7os d8 d8alt d8std
   ```

   If you have or are planning to have multiple VMs running using this script, you should be aware that you will need to change the name of your sites for each separate VM when you run setup.sh, and not rely on the defaults. That way your computer can route the sites correctly with the hosts file.

   In this case, your hosts file should look something like this:

   ```sh
   192.168.101.20 wcms-vagrant d7 d7alt d7rel d7lrel d7os d8 d8alt d8std
   192.168.101.21 wcms-vagrant1 test7 test7alt test8 test7rel test7lrel test7os test8 test8alt test8std
   192.168.101.35 wcms-vagrant2 p7 p7alt p7rel p7lrel p7os p8 p8alt p8std
   ```

   **Note** If you don't update the hosts file you won't be able to access the local development sites in a web browser.

5. Type:

   ```sh
   $ vagrant up
   ```

   This will create your dev environment, after several minutes it will complete and you should see:

   ```sh
   ==> default: ***************************************************************************************
   ==> default: The installation is now complete. You can access your VM via ssh vagrant@192.168.101.20
   ==> default: ***************************************************************************************
   ```

### Set up the local development server:

1. Log in over SSH using the IP address shown in the message at the end of the Vagrant setup step.

2. The login for your local development server is user: vagrant password: vagrant.

3. If you are planning to check out Gitlab repos using SSH instead of HTTP, you will need to add your Gitlab key to the VM. Here are the instructions from Gitlab for adding the key:

   https://git.uwaterloo.ca/help/ssh/README

4. You may need to add the LDAP config settings for LDAP to work. If you are part of the WCMS team you should already have access to the neccessary information. If you are an on-campus developer, you can contact the WCMS and ask how to set this up.

The folder is located here:

   ```sh
   $ cd /var/www/drupal7/config/
   ```
and the file is password-settings.php.

5. You will also need to add the WCMS fonts. If you are part of the WCMS team you should already have access to the neccessary information.
The fonts folder is located here:

   ```sh
   $ /var/www/drupal7/fonts
   ```
All the font folders are symlinked to this one, so putting the fonts here gives them to all the domains.

6. For Drupal 7, you should go to one of your Drupal folders, located at /var/www/drupal7/profiles/uw_base_profile (or drupal7alt) and pull down the latest version of the wcms base profile or the version you need to work with:

   ```sh
   $ git pull
   ```

   You should also do a rebuild of the profile to update any modules:

    ```sh
    $ ./rebuild.sh
    ```

7. Drupal 8 is installed, in drupal8, drupal8alt and drupal8std. Drupal8 and drupal8alt have both had composer install run so all you need to do is go to https://d8/fdsu1 (or whatever you named the server when you set it during creation) and follow the steps to install a site. Both drupal8 and drupal8alt were setup as multi-sites, drupal8std is a single site and you will need to run `composer install` to get everything setup.

8. There is also a drupal8pant dir, this has been included to allow for testing the Pantheon Drupal8 Composer build, this requires Composer 2 which is not currently installed on the VM.

9. Gesso can be found at https://<name-you-chose-for-vagrant>/uw_wcms_gesso/gesso/pattern-lab/ it is started by default during the creation of the VM.
e.g. https://wcms-vagrant/uw_wcms_gesso/gesso/pattern-lab

10. If you need to make changes to the composer.json file, either in /var/www/drupal8/ or /web/profiles/uw_base_profile you will need to run `composer update` in the top-level directory that has the composer.json file, so in the example /var/www/drupal8.

11. You should download the config for Drupal 8. In the /var/www/drupal8/web folder, clone the config repo from https://git.uwaterloo.ca/wcms/config.git and check out the wcms-3 branch. (If you do not have access to this repo, contact a WCMS developer who can do this for you.)

## Troubleshooting

1. If you run into an issue when using drush to clear caches the first time after you create a site, it could be that the permissions of the files folder for that site have changed. Either change the permissions for that specific file directory or just change the permissions at the /var/www level:

     ```sh
     $ sudo chown -R vagrant:www-data /var/www
     ```

2. Xdebug is enabled by default, if you need to disable it there is a script to do that:

    ```sh
    $ disable_xdebug.sh
    ```
   Which will disable xdebug. If you need to re-enable it:

    ```sh
    $ enable_xdebug.sh
    ```

3. Be aware that OpenScholar takes a lot of memory to install. The VM has 3072MB of memory allocated to it because of this, so running multiple VMs could cause performance issues.

4. You should periodically make sure that your dev environment is up to date.
   You can do that when you are logged in to your server by doing  an update and upgrade:

    ```sh
    $ sudo apt-get update

    $ sudo apt-get upgrade
    ```

5. To shutdown the VM type (from within the wcms-vagrant folder):

   ```sh
   $ vagrant halt
   ```

   To start it again you just type (from within the wcms-vagrant folder):

   ```sh
   $ vagrant up
   ```

6. If you don't want your VM any more type (from within the wcms-vagrant
   folder):

     ```sh
     $ vagrant destroy
     ```

   This will destroy your dev environment and remove it from VirtualBox but it keeps the settings you set when you ran **setup.sh**. If you want remove those settings as well then you need to run **clean.sh** which will remove all the config files and allow you to start again from scratch.

## Helpful aliases included in with the wcms-vagrant

Theses aliases can be found in the `~/.bash_aliases` file.
To use the commands, from the command line type the text on the left of the = to have the command on the right run:


 web = 'cd /var/www/'

 drupal7 = 'cd /var/www/drupal7'

 drupal7alt = 'cd /var/www/drupal7alt'

 drupal8 = 'cd /var/www/drupal8'

 drupal8alt = 'cd /var/www/drupal8alt'

 drupal8std = 'cd /var/www/drupal8std'

 ghtoken ='composer config --global github-oauth.github.com 1234567890'

 sites = 'cd /var/www/drupal7/sites/'

 profile = 'cd /var/www/drupal7/profiles/uw_base_profile'

 profilealt = 'cd /var/www/drupal7alt/profiles/uw_base_profile'

 profilerel = 'cd /var/www/drupal7rel/profiles/uw_base_profile'

 profilelrel = 'cd /var/www/drupal7lrel/profiles/uw_base_profile'

 profileos = 'cd /var/www/drupal7os/profiles/uw_base_profile'

 profile8 = 'cd /var/www/drupal8/web/profiles/uw_base_profile'

 profile8alt = 'cd /var/www/drupal8alt/web/profiles/uw_base_profile'

 profile8std = 'cd /var/www/drupal8std/web/profiles/uw_base_profile'

 alias drush7='~/drush7/vendor/bin/drush'

 alias drush8='~/drush8/vendor/bin/drush'

 alias drush9='~/drush9/vendor/bin/drush'

 alias drush10='~/drush10/vendor/bin/drush'

 defaultphp7.2 = 'defaultphp7.2.sh'

 defaultphp7.3 = 'defaultphp7.3.sh'

 fdsu1 = 'cd /var/www/drupal7/sites/<name of server>7.fdsu1'

 fdsu2 = 'cd /var/www/drupal7/sites/<name of server>7.fdsu2'

 fdsu3 = 'cd /var/www/drupal7/sites/<name of server>7.fdsu3'

 fdsu4 = 'cd /var/www/drupal7/sites/<name of server>7.fdsu4'

 fdsu5 = 'cd /var/www/drupal7/sites/<name of server>7.fdsu5'

 fdsu1alt = 'cd /var/www/drupal7alt/sites/<name of server>7alt.fdsu1'

 fdsu2alt = 'cd /var/www/drupal7alt/sites/<name of server>7alt.fdsu2'

 fdsu3alt = 'cd /var/www/drupal7alt/sites/<name of server>7alt.fdsu3'

 fdsu4alt = 'cd /var/www/drupal7alt/sites/<name of server>7alt.fdsu4'

 fdsu5alt = 'cd /var/www/drupal7alt/sites/<name of server>7alt.fdsu5'

 fdsu1rel = 'cd /var/www/drupal7rel/sites/<name of server>7rel.fdsu1'

 fdsu2rel = 'cd /var/www/drupal7rel/sites/<name of server>7rel.fdsu2'

 fdsu3rel = 'cd /var/www/drupal7rel/sites/<name of server>7rel.fdsu3'

 fdsu4rel = 'cd /var/www/drupal7rel/sites/<name of server>7rel.fdsu4'

 fdsu5rel = 'cd /var/www/drupal7rel/sites/<name of server>7rel.fdsu5'

 fdsu1os = 'cd /var/www/drupal7os/sites/<name of server>7os.fdsu1'

 fdsu2os = 'cd /var/www/drupal7os/sites/<name of server>7os.fdsu2'

 fdsu3os = 'cd /var/www/drupal7os/sites/<name of server>7os.fdsu3'

 fdsu4os = 'cd /var/www/drupal7os/sites/<name of server>7os.fdsu4'

 fdsu5os = 'cd /var/www/drupal7os/sites/<name of server>7os.fdsu5'

 fdsu1-8 = 'cd /var/www/drupal8/sites/<name of server>8.fdsu1'

 fdsu2-8 = 'cd /var/www/drupal8/sites/<name of server>8.fdsu2'

 fdsu3-8 = 'cd /var/www/drupal8/sites/<name of server>8.fdsu3'

 fdsu4-8 = 'cd /var/www/drupal8/sites/<name of server>8.fdsu4'

 fdsu5-8 = 'cd /var/www/drupal8/sites/<name of server>8.fdsu5'

 fdsu1-8alt = 'cd /var/www/drupal8alt/sites/<name of server>8alt.fdsu1'

 fdsu2-8alt = 'cd /var/www/drupal8alt/sites/<name of server>8alt.fdsu2'

 fdsu3-8alt = 'cd /var/www/drupal8alt/sites/<name of server>8alt.fdsu3'

 fdsu4-8alt = 'cd /var/www/drupal8alt/sites/<name of server>8alt.fdsu4'

 fdsu5-8alt = 'cd /var/www/drupal8alt/sites/<name of server>8alt.fdsu5'

 clr = 'clear'

 cc = 'drush cc all'

 updb = 'drush updb'

 makedrush = 'drush make --working-copy --force-gitinfofile --no-core -y --contrib-destination=. makefile/site.make'

 storegitpw='git config --global credential.helper store'

 si='drush si uw_base_profile --account-name=wcmsadmin --account-pass=a --site-name="WCMS Test"'
