echo ""
echo "Running drupal-general.sh"
echo ""

## All the things that are needed by both Drupal7 and Drupal8

## Change the permissions for /var/www
chown -R vagrant:www-data /var/www
chmod -R g+w /var/www

## Run updatedb which creates a system-wide database, to make searching easier
sudo updatedb

## Restart Apache
service apache2 restart

## Create a database and grant a user some permissions
echo "Create databases and grant permissions..."
dbprefaces='d7 d7alt d7rel d7os d8 d8alt d8std'

sites=5;
echo "start..."
for dbpreface in $dbprefaces
do
  for site in $(seq 1 $sites)
    do
    echo "Here are some variables $dbpreface"_fdsu"$site"
    echo "create database $dbpreface"_fdsu"$site;" | mysql -u root -p --password=vagrant
    echo "grant all on $dbpreface"_fdsu"$site.* to vagrant@localhost identified by 'vagrant';" | mysql -u root -p --password=vagrant
  done
done

## Create the Content Security Policy .conf file for the VM.
 # We have to add the VM "servers" to the list, we do that using sed and add them
## before we run the content-security-policy.php file.
cd /var/www/drupal7/profiles/uw_base_profile
sed -i s/\'uwaterloo.ca\',/\'uwaterloo.ca\',\\\n\ \ \'${vgrtserver}7\',\\\n\ \ \'${vgrtserver}7alt\',\\\n\ \ \'${vgrtserver}8\',\\\n\ \ \'${vgrtserver}8alt\',\\\n\ \ \'${vgrtserver}7rel\',\\\n\ \ \'${vgrtserver}7os\',/g content-security-policy.php
php content-security-policy.php > temp-php
## Remove the newline from the end of temp-php so we can add quotes to the end of it.
truncate -s -1 temp-php
echo -n \" >> temp-php
## create wcms-common.conf which contains header information including CSP.
echo -n "Header Always set X-UA-Compatible: \"IE=edge\"

# Added as DENY for RT#395748, changed after problems for RT#397113
Header Always set X-Frame-Options: \"SAMEORIGIN\"

# RT#395747
Header Always set X-Content-Type-Options: \"nosniff\"

# RT#395745: Content-Security-Policy
Header Always set Content-Security-Policy: \"" >> /etc/apache2/conf-available/wcms-common.conf

## Then we add the current CSP output (including our vms) to the file.
cat temp-php >> /etc/apache2/conf-available/wcms-common.conf

## enable the wcms-common config we created
a2enconf wcms-common

## Restart Apache
service apache2 restart

echo "Adding files via Vagrantfile for settings.local.yml..."
