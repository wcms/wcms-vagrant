echo ""
echo "Running finish.sh"
echo ""

## Call the variable file so we can fill in the bits
. /tmp/variable.sh


## Copy the development.services.yml files and settings.local.php we copied after running drupal.sh to all site folders.
## Need to run this here, as the files are loaded after the drupal.sh file runs.
echo ""
echo "Copying development.services.yml and settings.local from FDSU1 to the rest in the profile..."
echo ${vgrtserver}8.${vgrtsite}1
cp /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}1/development.services.yml /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}2
cp /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}1/settings.local.php /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}2
cp /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}1/development.services.yml /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}3
cp /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}1/settings.local.php /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}3
cp /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}1/development.services.yml /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}4
cp /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}1/settings.local.php /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}4
cp /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}1/development.services.yml /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}5
cp /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}1/settings.local.php /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}5


## Make vagrant the root password for mysql
## mysqladmin -u root password vagrant
echo ""
echo "Add passwords for mysql for vagrant user..."

echo 'phpmyadmin phpmyadmin/dbconfig-install boolean true' | debconf-set-selections
echo 'phpmyadmin phpmyadmin/app-password-confirm password vagrant' | debconf-set-selections
echo 'phpmyadmin phpmyadmin/mysql/admin-pass password vagrant' | debconf-set-selections
echo 'phpmyadmin phpmyadmin/mysql/app-pass password vagrant' | debconf-set-selections
echo 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2' | debconf-set-selections

sudo apt-get -q -y install phpmyadmin

## Add uw_wcms_tools to the local system
cd /home/vagrant
git clone https://git.uwaterloo.ca/wcms/uw_wcms_tools.git

## Make Vagrant the owner and apache the group for all the web folders
sudo chown -R vagrant:www-data /var/www
sudo chmod -R g+w /var/www
sudo chmod -R g+s /var/www/drupal8 /var/www/drupal8alt /var/www/drupal8std
#sudo chmod -R 775 /var/www/drupal8 /var/www/drupal8alt /var/www/drupal8std
sudo usermod -a -G www-data vagrant

## make sure that the vagrant home folder has the correct permissions
sudo chown -R vagrant:vagrant /home/vagrant/
sudo chown -R vagrant /var/www/drupal8/profile/uw_base_profile/modules/custom/uw_wcms_gesso/gesso

## add the wcms.crt to the ca-certificates.
sudo mv /tmp/WCMS.crt /usr/local/share/ca-certificates/WCMS.crt
sudo update-ca-certificates

## replace root with /home/vagrant so phpcs has proper drupal standards path
echo "
<?php
\$phpCodeSnifferConfig = [
  'installed_paths' => '/home/vagrant/.config/composer/vendor/drupal/coder/coder_sniffer',
];
?>" > /home/vagrant/.config/composer/vendor/squizlabs/php_codesniffer/CodeSniffer.conf

## Start building gesso so we can have it running when the VM is started
echo " Building Gesso..."
cd /var/www/drupal8/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso && ./build_gesso.sh

## Need to create a symlink for our uw_wcms_gesso folder to expose it for patternlab
echo "Create a symlink in the html folder so Gesso is exposed..."
cd /var/www/html && ln -s /var/www/drupal8/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso uw_wcms_gesso
cd /var/www/html && ln -s /var/www/drupal8alt/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso uw_wcms_gesso_alt

## Gesso can then be found at https://wcms-vagrant/uw_wcms_gesso/gesso/pattern-lab (or whatever your vagrant server is)
sudo chown -R vagrant:www-data /var/www
sudo chmod -R g+w /var/www


## Add aliases for drupal and site folders.
echo "
#my aliases

alias web='cd /var/www/'
alias drupal7='cd /var/www/drupal7'
alias drupal7alt='cd /var/www/drupal7alt'
alias drupal8='cd /var/www/drupal8'
alias drupal8alt='cd /var/www/drupal8alt'
alias drupal8std='cd /var/www/drupal8std'
alias sites='cd /var/www/drupal7/sites/'
alias ghtoken='composer config --global github-oauth.github.com ${vgrtgithubtoken}'
alias profile='cd /var/www/drupal7/profiles/uw_base_profile'
alias profilealt='cd /var/www/drupal7alt/profiles/uw_base_profile'
alias profilerel='cd /var/www/drupal7rel/profiles/uw_base_profile'
alias profileos='cd /var/www/drupal7os/profiles/uw_base_profile'
alias profile8='cd /var/www/drupal8/web/profiles/uw_base_profile'
alias profile8alt='cd /var/www/drupal8alt/web/profiles/uw_base_profile'
alias profile8std='cd /var/www/drupal8std'
alias gesso='cd /var/www/drupal8/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso'
alias gessoalt='cd /var/www/drupal8alt/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso'
alias gessogulp='cd /var/www/drupal8/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso/gesso && gulp build && cd -'
alias gessoaltgulp='cd /var/www/drupal8alt/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso/gesso && gulp build && cd -'
alias drush8='/usr/local/bin/drush'
alias drush9='~/drush9/vendor/bin/drush'
alias drush10='~/drush10/vendor/bin/drush'
alias defaultphp${VGRTPHPDRUPAL7}='defaultphp${VGRTPHPDRUPAL7}.sh'
alias defaultphp${VGRTPHPDRUPAL8}='defaultphp${VGRTPHPDRUPAL8}.sh'
alias enablexdebug='enable_xdebug.sh'
alias disablexdebug='disable_xdebug.sh'
alias ${vgrtsite}1='cd /var/www/drupal7/sites/${vgrtserver}7.${vgrtsite}1'
alias ${vgrtsite}2='cd /var/www/drupal7/sites/${vgrtserver}7.${vgrtsite}2'
alias ${vgrtsite}3='cd /var/www/drupal7/sites/${vgrtserver}7.${vgrtsite}3'
alias ${vgrtsite}4='cd /var/www/drupal7/sites/${vgrtserver}7.${vgrtsite}4'
alias ${vgrtsite}5='cd /var/www/drupal7/sites/${vgrtserver}7.${vgrtsite}5'
alias ${vgrtsite}1alt='cd /var/www/drupal7alt/sites/${vgrtserver}7alt.${vgrtsite}1'
alias ${vgrtsite}2alt='cd /var/www/drupal7alt/sites/${vgrtserver}7alt.${vgrtsite}2'
alias ${vgrtsite}3alt='cd /var/www/drupal7alt/sites/${vgrtserver}7alt.${vgrtsite}3'
alias ${vgrtsite}4alt='cd /var/www/drupal7alt/sites/${vgrtserver}7alt.${vgrtsite}4'
alias ${vgrtsite}5alt='cd /var/www/drupal7alt/sites/${vgrtserver}7alt.${vgrtsite}5'
alias ${vgrtsite}1rel='cd /var/www/drupal7rel/sites/${vgrtserver}7rel.${vgrtsite}1'
alias ${vgrtsite}2rel='cd /var/www/drupal7rel/sites/${vgrtserver}7rel.${vgrtsite}2'
alias ${vgrtsite}3rel='cd /var/www/drupal7rel/sites/${vgrtserver}7rel.${vgrtsite}3'
alias ${vgrtsite}4rel='cd /var/www/drupal7rel/sites/${vgrtserver}7rel.${vgrtsite}4'
alias ${vgrtsite}5rel='cd /var/www/drupal7rel/sites/${vgrtserver}7rel.${vgrtsite}5'
alias ${vgrtsite}1os='cd /var/www/drupal7os/sites/${vgrtserver}7os.${vgrtsite}1'
alias ${vgrtsite}2os='cd /var/www/drupal7os/sites/${vgrtserver}7os.${vgrtsite}2'
alias ${vgrtsite}3os='cd /var/www/drupal7os/sites/${vgrtserver}7os.${vgrtsite}3'
alias ${vgrtsite}4os='cd /var/www/drupal7os/sites/${vgrtserver}7os.${vgrtsite}4'
alias ${vgrtsite}5os='cd /var/www/drupal7os/sites/${vgrtserver}7os.${vgrtsite}5'
alias ${vgrtsite}1-8='cd /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}1'
alias ${vgrtsite}2-8='cd /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}2'
alias ${vgrtsite}3-8='cd /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}3'
alias ${vgrtsite}4-8='cd /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}4'
alias ${vgrtsite}5-8='cd /var/www/drupal8/web/sites/${vgrtserver}8.${vgrtsite}5'
alias ${vgrtsite}1-8alt='cd /var/www/drupal8alt/web/sites/${vgrtserver}8alt.${vgrtsite}1'
alias ${vgrtsite}2-8alt='cd /var/www/drupal8alt/web/sites/${vgrtserver}8alt.${vgrtsite}2'
alias ${vgrtsite}3-8alt='cd /var/www/drupal8alt/web/sites/${vgrtserver}8alt.${vgrtsite}3'
alias ${vgrtsite}4-8alt='cd /var/www/drupal8alt/web/sites/${vgrtserver}8alt.${vgrtsite}4'
alias ${vgrtsite}5-8alt='cd /var/www/drupal8alt/web/sites/${vgrtserver}8alt.${vgrtsite}5'
alias clr='clear'
alias cc='drush cc all'
alias cr='drush cr'
alias updb='drush updb'
alias makedrush='drush make --working-copy --force-gitinfofile --no-core -y --contrib-destination=. makefile/site.make'
alias storegitpw='git config --global credential.helper store'
alias si='si8'
alias startchromedriver='chromedriver --url-base=/wd/hub > /dev/null 2>&1 &'
LS_COLORS=$LS_COLORS:'di=0;35:' ; export LS_COLORS" > /home/vagrant/.bash_aliases
## increase the size of the git http buffer to prevent the remote end hung up unexpectedly
git config --global http.postBuffer 1048576000

echo "
## Function to run the right version of Drush depending on the folder you are in.

function drush ()
{
  ## Get enough of the current path to be able to tell if it's Drupal 8
  base=\${PWD:0:16}
  if [ \$base == \"/var/www/drupal8\" ]; then
    ~/drush10/vendor/bin/drush \"\${@}\"
  else
    /usr/local/bin/drush \$@
  fi
}

## Function to run correct version of drush si based on folder you are in.

function si8 ()
{
  ## Get enough of the current path to be able to tell if it's Drupal 8
  base=\${PWD:0:16}
  ## Set the base command that will be run regardless of Drupal version
  siteinstall=\"drush si uw_base_profile --account-name=wcmsadmin --account-pass=a --site-name=WCMS Test\"
  ## If the folder is a version that needs additional info, add that
  if [ \$base == \"/var/www/drupal8\" ]; then
    ## Basename gives us the last part of the path, and that's what we need to set sites-subdir to.
    tail=\$(basename \$PWD)
    siteinstall+=\" --sites-subdir=\$tail\"
  fi
  ## Run the command, and pass along any additional info that was passed at the command line (e.g. \"-y\")
  eval \$siteinstall \"\$@\"
}" >> /home/vagrant/.bashrc



## Create the defaultphp files so we can switch between php7.2 and 7.3
echo "
echo '-------------------------------------------------'
echo 'Changing version of PHP to ${VGRTPHPDRUPAL8}'
echo '-------------------------------------------------'
sudo a2dismod php${VGRTPHPDRUPAL7} && sudo a2enmod php${VGRTPHPDRUPAL8} && sudo update-alternatives --set php /usr/bin/php${VGRTPHPDRUPAL8} && sudo service apache2 restart
echo '-------------------------------------------------'
echo 'Apache has been restarted as part of the script.'
echo '-------------------------------------------------'" > /usr/local/bin/defaultphp${VGRTPHPDRUPAL8}.sh

sudo chown vagrant:root /usr/local/bin/defaultphp${VGRTPHPDRUPAL8}.sh
sudo chmod 754 /usr/local/bin/defaultphp${VGRTPHPDRUPAL8}.sh

echo "
echo '======================================================='
echo 'Changing version of PHP to ${VGRTPHPDRUPAL7}'
echo '======================================================='
sudo a2dismod php${VGRTPHPDRUPAL8} && sudo a2enmod php${VGRTPHPDRUPAL7} && sudo update-alternatives --set php /usr/bin/php${VGRTPHPDRUPAL7} && sudo service apache2 restart
echo '======================================================='
echo 'Apache has been restarted as part of the script.'
echo '======================================================='" > /usr/local/bin/defaultphp${VGRTPHPDRUPAL7}.sh

sudo chown vagrant:root /usr/local/bin/defaultphp${VGRTPHPDRUPAL7}.sh
sudo chmod 754 /usr/local/bin/defaultphp${VGRTPHPDRUPAL7}.sh

## Create a script to enable Xdebug.
echo "
echo '================================================================='
echo 'Enabling xdebug...'
echo '================================================================='
sudo sed -i \"s/;zend_extension=xdebug.so/zend_extension=xdebug.so/\" /etc/php/${VGRTPHPDRUPAL7}/mods-available/xdebug.ini
sudo sed -i \"s/;zend_extension=xdebug.so/zend_extension=xdebug.so/\" /etc/php/${VGRTPHPDRUPAL8}/mods-available/xdebug.ini
echo '================================================================='
echo 'Xdebug should now be enabled.'
echo '================================================================='" > /usr/local/bin/enable_xdebug.sh

sudo chown vagrant:root /usr/local/bin/enable_xdebug.sh
sudo chmod 754 /usr/local/bin/enable_xdebug.sh

## Create a script to disable Xdebug.
echo "
echo '================================================================='
echo 'Disabling xdebug...'
echo '================================================================='
sudo sed -i \"s/zend_extension=xdebug.so/;zend_extension=xdebug.so/\" /etc/php/${VGRTPHPDRUPAL7}/mods-available/xdebug.ini
sudo sed -i \"s/zend_extension=xdebug.so/;zend_extension=xdebug.so/\" /etc/php/${VGRTPHPDRUPAL8}/mods-available/xdebug.ini
echo '================================================================='
echo 'Xdebug should now be disabled.'
echo '================================================================='" > /usr/local/bin/disable_xdebug.sh

sudo chown vagrant:root /usr/local/bin/disable_xdebug.sh
sudo chmod 754 /usr/local/bin/disable_xdebug.sh

## Create script to copy config directory and add symlinks.
echo "
DIR=/var/www/drupal8/web/config
if [ -d "\$DIR" ]; then
   echo '==============================================================='
   echo 'Directory exists, no need to create a config dir...'
else
   echo '==============================================================='
   echo 'Cloning the config repo wcms-3 branch, you will need to supply you Git username when prompted...'
   cd /var/www/drupal8
   git clone https://git.uwaterloo.ca/wcms/config.git --branch wcms-3
   mv /var/www/drupal8/config/simplesamlphp /var/www/drupal8/simplesamlphp
   mv /var/www/drupal8/config /var/www/drupal8/web/config
   cd /var/www/drupal8/vendor/simplesamlphp/simplesamlphp/ && ln -s ../../../simplesamlphp/config config
fi
echo '==============================================================='" > /usr/local/bin/get-config.sh

sudo chown vagrant:root /usr/local/bin/get-config.sh
sudo chmod 754 /usr/local/bin/get-config.sh

## add a script to create the symlinks for conference and publication sites
echo "
#
# Script to create symlinks for publication and conference themes (apps folder)
#

if [ \$# -eq 0 ]
then
        echo "Missing parameter!"
        echo "create_theme_symlinks.sh DRUPAL-ROOT"
        echo ""
        exit 0
else
       ln -s /var/www/\$1/profiles/uw_base_profile/themes/uw_theme_publication /var/www/\$1/apps/uw_publication
       ln -s /var/www/\$1/profiles/uw_base_profile/themes/uw_theme_conference /var/www/\$1/apps/uw_theme_conference
fi

echo "Links created!"
" > /usr/local/bin/create_theme_symlinks.sh

sudo chown vagrant:root /usr/local/bin/create_theme_symlinks.sh
sudo chmod 754 /usr/local/bin/create_theme_symlinks.sh

## Make sure the defaultphp is set correctly
defaultphp${VGDEFAULTPHPVERSION}.sh

echo "
===================================================================================
Making sure everything is up to date... If the update requires manual interaction
it may hang and you will not see the Installation is now complete message.
If the update does hang, all the required bits are installed and you can access
your VM via ssh vagrant@${vgrtip}.
You may need to reboot the VM (sudo reboot) to fix an issue with the package that
required manual interaction. When it is up run 'sudo dpkg --configure -a' inside
your VM to continue to install the package.
===================================================================================
"
sudo chown -R vagrant:vagrant /home/vagrant/

## Make sure everything is up to date. If there are a lot, or they require interaction it probably means you need to run vagrant update box

## sudo apt-get upgrade --assume-yes --allow-unauthenticated

## Periodically, the box we use seems to include updates for the grub boot loader which requires interaction and hangs the install.
# The code below seems to get around that by forcing certain options. We should generally use the code above and if it fails the one below.
# We should review periodically (or when the box gets updated) to check that the upgrade code above works and switch it.
##
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade

echo "
-----------------------------------------------------------------------------------
If you don't want to enter your git username and password you can run:

storegitpw

The next time you commit something to git it will store your username and password
in a plain text file on the VM and use it to supply credentials for future commits
-----------------------------------------------------------------------------------
"

echo "
***********************************************************************************
The installation is now complete. You can access your VM via ssh vagrant@${vgrtip}
***********************************************************************************
"
## After an install it sometimes says that a restart is required, this should fix that:
sudo reboot
