echo ""
echo "Running drupal8.sh"
echo ""

drupal8_version='8.9.19'

profiles_D8='drupal8 drupal8alt drupal8std'
profiles_D8fdsu='drupal8 drupal8alt'

## Call the variable file so we can fill in the bits
. /tmp/variable.sh

for profile in $profiles_D8fdsu
  do
    echo ""
    echo "-----------------"
    echo "Installing d8 in ""$profile"
    pwd
    echo "-----------------"
    #drush dl -v -d "$drupal8_version" --destination="/var/www" --drupal-project-rename="$profile"
    git clone --branch 3.1.x https://git.uwaterloo.ca/wcms/drupal-recommended-project.git /var/www/"$profile"
  done

## Clone Drupal8 recommended-project into the drupalstd space.
git clone https://github.com/drupal/recommended-project.git /var/www/drupal8std

## Clone our "new" Pantheon Composer build into Drupal8pant
git clone https://git.uwaterloo.ca/wcms-automation/wcms-pantheon-composer.git /var/www/drupal8pant

## Install all the things just for the drupal8 profile and drupalstd, so the install doesn't take forever...
sudo su - vagrant && cd /var/www/drupal8 && ./rebuild.sh 1
echo "Ran rebuild script..."
cd /var/www/drupal8/web/sites || exit
echo "Sites folder exists..."

sudo su - vagrant && cd /var/www/drupal8alt && ./rebuild.sh 1
echo "Ran rebuild script..."
cd /var/www/drupal8alt/web/sites || exit
echo "Sites folder exists..."

## Create the config folder in drupal root and an empty password_settings file

## We are now pulling the config dir in from https://git.uwaterloo.ca/wcms/config directory
## because its protected we need to do it after the build.

#mkdir /var/www/drupal8/web/config
#touch /var/www/drupal8/web/config/password_settings.php
#echo "<?php \$config['samlauth.authentication']['sp_entity_id'] = \$UWhost . '.' . \$UWpref;" >> /var/www/drupal8/config/password_settings.php

ln -s /var/www/drupal8/web/config /var/www/drupal8alt/web/config
ln -s /var/www/drupal8/web/config/fonts /var/www/drupal8/web/fonts
ln -s /var/www/drupal8alt/web/config/fonts /var/www/drupal8alt/web/fonts

## Do the same for d8 sites using the updated /web path.
echo ""
echo "----------------"
echo "Creating D8 sites...."
cd /var/www/
pwd
echo "-----------------"

sites=5;
for profile in $profiles_D8fdsu
do
   sitename="${profile/drupal/${vgrtserver}}"
   for site in $(seq 1 $sites)
   do
    echo ""
    echo "----------------"
    echo "Creating D8 sites in /var/www/" $profiles_d8
    pwd
    echo "-----------------"
    mkdir /var/www/$profile/web/sites/$sitename.${vgrtsite}$site
    mkdir /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/files
    mkdir /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/files/temp
    mkdir /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/files/public
    mkdir /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/files/private
    mkdir /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/modules
    echo "${vgrtsite}$site"
   done
done

echo "Create symlinks for multi-site for D8"
## Create symlinks for multi-site
sites=5;
for profile in $profiles_D8fdsu
do
  for site in $(seq 1 $sites)
  do
    cd /var/www/"$profile"/web || exit
    ln -s . "${vgrtsite}$site"
  done
done


## Drupal 8 requires a sync folder to be created
echo 'Creating Drupal 8 sync directory......'
sites=5;
for profile in $profiles_D8
do
 sitename="${profile/drupal/${vgrtserver}}"
 ## Create the sites.php file for drupal8 with the fdsu sites defined otherwise the setting.php can't be found.
  echo "<?php
    \$sites = array(
        '$sitename.${vgrtsite}1' => '${vgrtsite}1',
        '$sitename.${vgrtsite}2' => '${vgrtsite}2',
        '$sitename.${vgrtsite}3' => '${vgrtsite}3',
        '$sitename.${vgrtsite}4' => '${vgrtsite}4',
        '$sitename.${vgrtsite}5' => '${vgrtsite}5',
      );" > /var/www/$profile/web/sites/sites.php
   echo "created $profile sites.php file."
   for site in $(seq 1 $sites)
   do
    mkdir /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/files/config_acqTmp
    mkdir /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/files/config_acqTmp/sync
    echo "$site"
   done
done

## We are now pulling this in from https://git.uwaterloo.ca/wcms/config directory
## because its protected we need to do it after the build.

## Create the symlink for saml_config so it works with /web docroot.
#for profile in $profiles_D8fdsu
#do
#  ln -s /var/www/$profile/web/profiles/uw_base_profile/saml_config /var/www/$profile/web/saml_config
#done

echo "Create symlinks for D8 sites"
## Create the Drupal8 settings.php file. It's a bit different because we don't have a base profile yet! (Mar.2016)
sites=5;
for profile in $profiles_D8fdsu
do
  sitename="${profile/drupal/${vgrtserver}}"
  server="${profile/drupal/d}"
  for site in $(seq 1 $sites)
   do
     ## Drupal 8 requires a salted hash in the settings.php file (if it's missing it rewrites the database settings to the file).
     salt_hash=$(echo $(date +%s) $site | sha256sum | base64 | head -c 74);
    echo "<?php
    // Server domain name.
    ${VGUWHOST} = '$sitename';
    // Path to site root.
    ${VGUWPREF} = '${vgrtsite}$site';
    // Name of site database.
    ${VGUWDB} = '$server"_fdsu"$site';
    // Username to connect to site database.
    ${VGUWUSER} = 'root';
    // Password to connect to site database.
    ${VGUWPASS} = 'vagrant';
    // Host name to connect to site database.
    ${VGUWPRI} = 'localhost';

    require_once(DRUPAL_ROOT . '/profiles/uw_base_profile/drupal-settings.php');

    // File system settings.
    ${VGUWCONFD8}['file_default_scheme'] = 'public';
    ${VGUWCONFD8}['file_private_path'] = 'sites/$sitename.${vgrtsite}$site/files/private/';
    ${VGUWCONFD8}['file_public_path'] = 'sites/$sitename.${vgrtsite}$site/files';
    ${VGUWCONFD8}['file_temporary_path'] = 'sites/$sitename.${vgrtsite}$site/files/temp/';
    ${VGUWCONFD8}['file_chmod_directory'] = 02775;
    ${VGUWCONFD8}['file_chmod_file'] = 0664;

    // D8 required settings.
    ${VGUWCONFD8}['hash_salt'] = '$salt_hash';
    ${VGUWCONFD8}['trusted_host_patterns'] = ['^$sitename\$',];
    ${VGUWCONFD8}['config_sync_directory'] = 'sites/$sitename.${vgrtsite}$site/files/config_acqTmp/sync';

    // Automatically enable the dev module as this is development site.
    \$settings['uw_dev_site'] = TRUE;

    if (file_exists(__DIR__ . '/settings.local.php')) {
        include __DIR__ . '/settings.local.php';
    }" > /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/settings.php

    echo "Copy the services.yml file for all D8 sites..."
    cp /var/www/drupal8/web/sites/default/default.services.yml /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/services.yml
  done
done

## Add the phpunit.xml.dist file to drupal 8 profiles for phpunit testing.
echo "Adding phpunit files for D8..."
sites=5;
for profile in $profiles_D8fdsu
  do
    sitename="${profile/drupal/${vgrtserver}}"
    server="${profile/drupal/d}"
    for site in $(seq 1 $sites)
      do
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>

        <!-- TODO set checkForUnintentionallyCoveredCode=\"true\" once https://www.drupal.org/node/2626832 is resolved. -->
        <!-- PHPUnit expects functional tests to be run with either a privileged user
        or your current system user. See core/tests/README.md and
        https://www.drupal.org/node/2116263 for details.
        -->
        <phpunit bootstrap=\"../../core/tests/bootstrap.php\" colors=\"true\"
                beStrictAboutTestsThatDoNotTestAnything=\"true\"
                beStrictAboutOutputDuringTests=\"true\"
                beStrictAboutChangesToGlobalState=\"true\"
                checkForUnintentionallyCoveredCode=\"false\">
        <!-- TODO set printerClass=\"\Drupal\Tests\Listeners\HtmlOutputPrinter\" once
        https://youtrack.jetbrains.com/issue/WI-24808 is resolved. Drupal provides a
        result printer that links to the html output results for functional tests.
        Unfortunately, this breaks the output of PHPStorm's PHPUnit runner. However, if
        using the command line you can add
        - -printer=\"\Drupal\Tests\Listeners\HtmlOutputPrinter\" to use it (note there
        should be no spaces between the hyphens).
        -->
          <php>
            <!-- Set error reporting to E_ALL. -->
            <ini name=\"error_reporting\" value=\"32767\"/>
            <!-- Do not limit the amount of memory tests take to run. -->
            <ini name=\"memory_limit\" value=\"-1\"/>
            <!-- Example \"SIMPLETEST_BASE_URL\" value: http://localhost -->
            <env name=\"SIMPLETEST_BASE_URL\" value=\"https://$sitename/${vgrtsite}$site\"/>
            <!-- Example \"SIMPLETEST_DB\" value: mysql://username:password@localhost/databasename#table_prefix -->
            <env name=\"SIMPLETEST_DB\" value=\"mysql://root:vagrant@localhost/$sitename\_${vgrtsite}$site\"/>
            <!-- Example BROWSERTEST_OUTPUT_DIRECTORY value: /path/to/webroot/sites/simpletest/browser_output -->
            <env name=\"BROWSERTEST_OUTPUT_DIRECTORY\" value=\"\"/>
          </php>
          <testsuites>
            <testsuite name=\"unit\">
              <file>../../core/tests/TestSuites/UnitTestSuite.php</file>
            </testsuite>
            <testsuite name=\"kernel\">
              <file>../../core/TestSuites/KernelTestSuite.php</file>
            </testsuite>
            <testsuite name=\"functional\">
              <file>../../core/tests/TestSuites/FunctionalTestSuite.php</file>
            </testsuite>
            <testsuite name=\"functional-javascript\">
              <file>../../core/tests/TestSuites/FunctionalJavascriptTestSuite.php</file>
            </testsuite>
          </testsuites>
          <listeners>
            <listener class=\"\Drupal\Tests\Listeners\DrupalStandardsListener\">
            </listener>
          </listeners>
          <!-- Filter for coverage reports. -->
          <filter>
            <whitelist>
              <directory>../../core/includes</directory>
              <directory>../../core/lib</directory>
              <directory>../../core/modules</directory>
              <directory>../../../modules</directory>
              <directory>../../../sites</directory>
              <!-- By definition test classes have no tests. -->
              <exclude>
                <directory suffix=\"Test.php\">./</directory>
                <directory suffix=\"TestBase.php\">./</directory>
              </exclude>
            </whitelist>
          </filter>
        </phpunit>
        " > /var/www/$profile/web/sites/$sitename.${vgrtsite}$site/phpunit.xml.dist
    done
    echo "Done copying phpunit.xml"
  done
