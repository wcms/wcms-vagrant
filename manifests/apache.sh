echo ""
echo "Running apache.sh"
echo ""
## Call the variable file so we can fill in the bits
. /tmp/variable.sh
## Create self-signed certs for the secure domains
cd /tmp || return

## Create an Apache vhost
profiles="drupal7 drupal7alt drupal7rel drupal7os"
for profile in $profiles
  do
   server="${profile/drupal/${vgrtserver}}"
  echo "$server"
  echo "
  <VirtualHost *:80>
      DocumentRoot /var/www/$profile
      ServerAdmin webmaster@localhost
      ServerName $server
      Redirect permanent / https://$server/
        <Directory />
            Options FollowSymLinks
            AllowOverride None
        </Directory>
        <Directory /var/www/>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
        </Directory>
    </VirtualHost>

   <VirtualHost *:443>
      ServerAdmin webmaster@localhost
      DocumentRoot /var/www/$profile
      ServerName $server
      <Directory />
          Options FollowSymLinks
          AllowOverride None
      </Directory>
      <Directory /var/www/>
          Options Indexes FollowSymLinks MultiViews
          AllowOverride All
          Order allow,deny
          allow from all
      </Directory>

       ErrorLog ${APACHE_LOG_DIR}/error.log

      # Possible values include: debug, info, notice, warn, error, crit,
      # alert, emerg.
      LogLevel warn

      CustomLog ${APACHE_LOG_DIR}/access.log combined

        SSLEngine on
          SSLCertificateFile    /etc/ssl/certs/server.crt
          SSLCertificateKeyFile /etc/ssl/private/server.key

      Alias /doc/ /usr/share/doc/
      <Directory /usr/share/doc/>
          Options Indexes MultiViews FollowSymLinks
          AllowOverride None
          Order deny,allow
          Deny from all
          Allow from 127.0.0.0/255.0.0.0 ::1/128
      </Directory>

  </VirtualHost>" > /etc/apache2/sites-available/"$server".conf
done

profiles="drupal8 drupal8alt drupal8std drupal8pant"
for profile in $profiles
  do
   server="${profile/drupal/${vgrtserver}}"
  echo "$server"
  echo "
  <VirtualHost *:80>
      DocumentRoot /var/www/$profile/web
      ServerAdmin webmaster@localhost
      ServerName $server
      Redirect permanent / https://$server/
        <Directory />
            Options FollowSymLinks
            AllowOverride None
        </Directory>
        <Directory /var/www/>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
        </Directory>
    </VirtualHost>

   <VirtualHost *:443>
      ServerAdmin webmaster@localhost
      DocumentRoot /var/www/$profile/web
      ServerName $server
      <Directory />
          Options FollowSymLinks
          AllowOverride None
      </Directory>
      <Directory /var/www/>
          Options Indexes FollowSymLinks MultiViews
          AllowOverride All
          Order allow,deny
          allow from all
      </Directory>

       ErrorLog ${APACHE_LOG_DIR}/error.log

      # Possible values include: debug, info, notice, warn, error, crit,
      # alert, emerg.
      LogLevel warn

      CustomLog ${APACHE_LOG_DIR}/access.log combined

        SSLEngine on
          SSLCertificateFile    /etc/ssl/certs/server.crt
          SSLCertificateKeyFile /etc/ssl/private/server.key

      Alias /doc/ /usr/share/doc/
      <Directory /usr/share/doc/>
          Options Indexes MultiViews FollowSymLinks
          AllowOverride None
          Order deny,allow
          Deny from all
          Allow from 127.0.0.0/255.0.0.0 ::1/128
      </Directory>

  </VirtualHost>" > /etc/apache2/sites-available/"$server".conf
done
## rename the default ssl file to 000-default-ssl to match the 000-default.conf
## so when no server is specified it falls back to the ip.
mv /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/000-default-ssl.conf

## Tell Apache about our vhosts
a2ensite "${vgrtserver}"7.conf "${vgrtserver}"7alt.conf "${vgrtserver}"8.conf "${vgrtserver}"8alt.conf "${vgrtserver}"8std.conf \
"${vgrtserver}"8pant.conf 000-default-ssl.conf "${vgrtserver}"7rel.conf "${vgrtserver}"7os.conf


## Tweak permissions for www-data user
chgrp www-data /var/log/apache2
chmod g+w /var/log/apache2
chown -R vagrant:www-data /var/www
chmod -R g+w /var/www
