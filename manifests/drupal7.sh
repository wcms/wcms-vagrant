echo ""
echo "Running drupal7.sh"
echo ""

## Set the Drupal versions in one place
drupal7_version='7.82'

## Install our base profile
profiles_D7='drupal7 drupal7alt drupal7rel drupal7os'
profiles_fdsu_D7='drupal7 drupal7alt drupal7rel'

## Call the variable file so we can fill in the bits
. /tmp/variable.sh

echo "Installing Drupal in ""$profile"
cd /var/www || return

for profile in $profiles_D7
  do
    drush dl -v -d drupal-$drupal7_version --destination="/var/www" --drupal-project-rename="$profile"
done

for profile in $profiles_fdsu_D7
do
  cd /var/www/$profile/profiles
  git clone https://git.uwaterloo.ca/wcms/uw_base_profile.git
done

cd /var/www/drupal7os/profiles
git clone -b 7.x-3.x --single-branch https://git.uwaterloo.ca/wcms-openscholar/openscholar

cd /var/www/drupal7rel/profiles/uw_base_profile
git checkout 7.x-2.9.1

## Install the drush registry_rebuild module
drush @none dl registry_rebuild

## Create the config folder in drupal root and an empty password_settings file
mkdir /var/www/drupal7/config
touch /var/www/drupal7/config/password_settings.php


## Create an empty fonts folder so I know where it's supposed to go! ;)
mkdir /var/www/drupal7/fonts

for profile in $profiles_D7
do
  ln -s /var/www/drupal7/config /var/www/$profile/
  ln -s /var/www/drupal7/fonts /var/www/$profile/
done

## Create site folders for drupal multi-site
## Use string replace to get the "bit after drupal" to complete the sitename to reduce code.
echo 'Creating site directories......'

sites=5;
for profile in $profiles_fdsu_D7
do
   sitename="${profile/drupal/${vgrtserver}}"
   for site in $(seq 1 $sites)
   do
    mkdir /var/www/$profile/sites/$sitename.${vgrtsite}$site
    mkdir /var/www/$profile/sites/$sitename.${vgrtsite}$site/files
    mkdir /var/www/$profile/sites/$sitename.${vgrtsite}$site/files/temp
    mkdir /var/www/$profile/sites/$sitename.${vgrtsite}$site/files/public
    mkdir /var/www/$profile/sites/$sitename.${vgrtsite}$site/files/private
    mkdir /var/www/$profile/sites/$sitename.${vgrtsite}$site/modules
    echo "${vgrtsite}$site"
   done
done


echo "Create symlinks for multi-site for D7"
## Create symlinks for multi-site
sites=5;
for profile in $profiles_D7
do
  for site in $(seq 1 $sites)
  do
    cd /var/www/"$profile" || exit
    ln -s . "${vgrtsite}$site"
  done
done

##Create settings.php files for each site
sites=5;
for profile in $profiles_D7
do
  sitename="${profile/drupal/${vgrtserver}}"
  server="${profile/drupal/d}"
  for site in $(seq 1 $sites)
    do
      echo "<""?php
      // Server domain name.
      ${VGUWHOST} = '$sitename';
      // Path to site root.
      ${VGUWPREF} = '${vgrtsite}$site';
      // Name of site database.
      ${VGUWDB} = '$server"_fdsu"$site';
      // Username to connect to site database.
      ${VGUWUSER} = 'root';
      // Password to connect to site database.
      ${VGUWPASS} = 'vagrant';
      // Host name to connect to site database.
      ${VGUWPRI} = 'localhost';

      // Path to \"global\" settings file. Do not edit.
      require_once(DRUPAL_ROOT . '/profiles/uw_base_profile/drupal-settings.php');

      // File system settings.
      ${VGUWCONF}['file_default_scheme'] = 'public';
      ${VGUWCONF}['file_private_path'] = 'sites/$sitename.${vgrtsite}$site/files/private/';
      ${VGUWCONF}['file_public_path'] = 'sites/$sitename.${vgrtsite}$site/files';
      ${VGUWCONF}['file_temporary_path'] = 'sites/$sitename.${vgrtsite}$site/files/temp/';
      ${VGUWCONF}['file_chmod_directory'] = 02775;
      ${VGUWCONF}['file_chmod_file'] = 0664;" > /var/www/$profile/sites/$sitename.${vgrtsite}$site/settings.php
    done
done

## Add the apps folder for Drupal 7 conference and publications themes.
for profile in $profiles_D7
do
  mkdir /var/www/$profile/apps
done

echo "Setting up all the things for Drupal8..."
