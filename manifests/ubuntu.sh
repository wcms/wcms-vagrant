echo ""
echo "Running ubuntu.sh"
echo ""
## Return the date so we can see how long the ubuntu.sh file runs.
date
echo ""
## Make sure that we are starting fresh with the repositories
sudo apt-get clean
## Call the variable file so we can fill in the bits
. /tmp/variable.sh

## This file gets executed the first time you do a 'vagrant up', if you want it to
## run again you'll need run 'vagrant provision'.

## Install all the ubuntu things

## Update the /etc/apt/sources.list to use the uwaterloo CSClub mirror
echo "## Note, this file is written by cloud-init on first boot of an instance
## modifications made here will not survive a re-bundle.
## if you wish to make changes you can:
## a.) add 'apt_preserve_sources_list: true' to /etc/cloud/cloud.cfg
##     or do the same in user-data
## b.) add sources in /etc/apt/sources.list.d
## c.) make changes to template file /etc/cloud/templates/sources.list.tmpl
#

# See http://help.ubuntu.com/community/UpgradeNotes for how to upgrade to
# newer versions of the distribution.
# deb http://archive.ubuntu.com/ubuntu bionic main restricted
# deb-src http://archive.ubuntu.com/ubuntu bionic main restricted
deb https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic main restricted
deb-src https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic main restricted

## Major bug fix updates produced after the final release of the
## distribution.
# deb http://archive.ubuntu.com/ubuntu bionic-updates main restricted
# deb-src http://archive.ubuntu.com/ubuntu bionic-updates main restricted
deb https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-updates main restricted
deb-src https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-updates main restricted

## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu
## team. Also, please note that software in universe WILL NOT receive any
## review or updates from the Ubuntu security team.
# deb http://archive.ubuntu.com/ubuntu bionic universe
# deb-src http://archive.ubuntu.com/ubuntu bionic universe
# deb http://archive.ubuntu.com/ubuntu bionic-updates universe
# deb-src http://archive.ubuntu.com/ubuntu bionic-updates universe
deb https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic universe
deb-src https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic universe
deb https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-updates universe
deb-src https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-updates universe

## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu
## team, and may not be under a free licence. Please satisfy yourself as to
## your rights to use the software. Also, please note that software in
## multiverse WILL NOT receive any review or updates from the Ubuntu
## security team.
# deb http://archive.ubuntu.com/ubuntu bionic multiverse
# deb-src http://archive.ubuntu.com/ubuntu bionic multiverse
# deb http://archive.ubuntu.com/ubuntu bionic-updates multiverse
# deb-src http://archive.ubuntu.com/ubuntu bionic-updates multiverse
deb https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic multiverse
deb-src https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic multiverse
deb https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-updates multiverse
deb-src https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-updates multiverse

## Uncomment the following two lines to add software from the 'backports'
## repository.
## N.B. software from this repository may not have been tested as
## extensively as that contained in the main release, although it includes
## newer versions of some applications which may provide useful features.
## Also, please note that software in backports WILL NOT receive any review
## or updates from the Ubuntu security team.
# deb http://archive.ubuntu.com/ubuntu bionic-backports main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu bionic-backports main restricted universe multiverse
deb https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-backports main restricted universe multiverse
deb-src https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-backports main restricted universe multiverse

## Uncomment the following two lines to add software from Canonical's
## 'partner' repository.
## This software is not part of Ubuntu, but is offered by Canonical and the
## respective vendors as a service to Ubuntu users.
# deb http://archive.canonical.com/ubuntu bionic partner
# deb-src http://archive.canonical.com/ubuntu bionic partner

# deb http://security.ubuntu.com/ubuntu bionic-security main
# deb-src http://security.ubuntu.com/ubuntu bionic-security main
deb https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-security main
deb-src https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-security main
# deb http://security.ubuntu.com/ubuntu bionic-security universe
# deb-src http://security.ubuntu.com/ubuntu bionic-security universe
deb https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-security universe
deb-src https://mirror.csclub.uwaterloo.ca/ubuntu/ bionic-security universe
# deb http://security.ubuntu.com/ubuntu bionic-security multiverse
# deb-src [arch=i386,amd64] http://mariadb.mirror.anstey.ca/repo/10.1/ubuntu bionic main
# deb-src http://security.ubuntu.com/ubuntu bionic-security multiverse" > /etc/apt/sources.list

## Add phpmyadmin so it will install the latest version that works with php 7.0
#deb http://ppa.launchpad.net/nijel/phpmyadmin/ubuntu bionic main
#deb-src http://ppa.launchpad.net/nijel/phpmyadmin/ubuntu bionic main

## For ubuntu bionic we need to add some repos for the latest PHP version
export DEBIAN_FRONTEND=noninteractive
## Since setting the timezone above doesn't seem to do anything...
echo "setting timezone..."
sudo timedatectl set-timezone America/Toronto
sudo apt-get update
## Changed apt-get upgrade to this because grub loader can break an install
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade
sudo apt-get install apt-transport-https gnupg2 software-properties-common gcc-6-base libasound2 libasound2-data --assume-yes
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository -y ppa:ondrej/php
## Add the git repo so we can get the latest git (we need 2.9.2+)
sudo add-apt-repository ppa:git-core/ppa

## Add PPA for phpmyadmin to get version 5.
sudo add-apt-repository ppa:phpmyadmin/ppa

## pdftk is no longer part of Ubuntu 18.04 repo to add it:
echo "Adding pdftk manually:"
wget http://ftp.br.debian.org/debian/pool/main/p/pdftk/pdftk_2.02-4+b2_amd64.deb && \
wget http://ftp.br.debian.org/debian/pool/main/g/gcc-defaults/libgcj-common_6.3-4_all.deb && \
wget http://ftp.br.debian.org/debian/pool/main/g/gcc-6/libgcj17_6.3.0-18+deb9u1_amd64.deb && \
dpkg -i pdftk_2.02-4+b2_amd64.deb libgcj17_6.3.0-18+deb9u1_amd64.deb libgcj-common_6.3-4_all.deb

apt-get clean

## Added so we can install yarn for Drupal8 theme work.
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

## Need to use this repository, otherwise we get errors with Drupal8 later
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
echo "Updated node repo..."

## Pre-configure mysql username and password
echo "mysql-server mysql-server/root_password password vagrant" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password vagrant" | sudo debconf-set-selections

## Pre-configure responses for postfix install
echo "postfix postfix/mailname string $server" | sudo debconf-set-selections
## Options are: No configuration , Internet site, Internet with smarthost, Satellite system, Local only
echo "postfix postfix/main_mailer_type string 'Internet Site'" | sudo debconf-set-selections

sudo apt-get update --assume-yes
sudo apt-get upgrade --assume-yes

echo "Drupal7-PHP"${VGRTPHPDRUPAL7}
echo "Drupal8-PHP"${VGRTPHPDRUPAL8}
echo "Default-PHP"${VGDEFAULTPHPVERSION}

## Install php7.2 and php7.3 versions so we can switch when needed.
sudo apt-get install --assume-yes php${VGRTPHPDRUPAL7} apache2 php${VGRTPHPDRUPAL8} php${VGRTPHPDRUPAL8}-xml mysql-server-5.7 mysql-client-5.7

sudo apt-get install --assume-yes vim php-pear libpcre2-8-0 git-man ruby-sass ruby-compass \
ruby-all-dev yarn dos2unix optipng jpegoptim imagemagick libapache2-mod-fcgid libjpeg8 anacron \
libapache2-mod-php${VGRTPHPDRUPAL7} php${VGRTPHPDRUPAL7}-xml php${VGRTPHPDRUPAL7}-fpm php${VGRTPHPDRUPAL7}-mbstring \
php${VGRTPHPDRUPAL7}-mysql php${VGRTPHPDRUPAL7}-zip php${VGRTPHPDRUPAL7}-gd php${VGRTPHPDRUPAL7} php${VGRTPHPDRUPAL7}-common \
php${VGRTPHPDRUPAL7}-fpm php${VGRTPHPDRUPAL7}-apc php${VGRTPHPDRUPAL7}-mysql php${VGRTPHPDRUPAL7}-mbstring php${VGRTPHPDRUPAL7}-gettext \
php${VGRTPHPDRUPAL7}-simplexml php${VGRTPHPDRUPAL7}-xdebug php${VGRTPHPDRUPAL7}-sqlite3 php${VGRTPHPDRUPAL7}-gd php${VGRTPHPDRUPAL7}-cli \
php${VGRTPHPDRUPAL7}-curl php${VGRTPHPDRUPAL7}-ldap php${VGRTPHPDRUPAL7}-cgi php${VGRTPHPDRUPAL7}-dev php${VGRTPHPDRUPAL7}-xmlwriter \
curl php${VGRTPHPDRUPAL8}-cli php${VGRTPHPDRUPAL8}-ldap php${VGRTPHPDRUPAL8}-mbstring php${VGRTPHPDRUPAL8}-xml php${VGRTPHPDRUPAL8}-xmlwriter \
php${VGRTPHPDRUPAL8}-curl php${VGRTPHPDRUPAL8}-gd php${VGRTPHPDRUPAL8}-mysql php${VGRTPHPDRUPAL7}-zip php${VGRTPHPDRUPAL8}-zip \
php${VGRTPHPDRUPAL8}-sqlite3 php${VGRTPHPDRUPAL8}-xdebug php${VGRTPHPDRUPAL7}-xdebug php${VGRTPHPDRUPAL8}-fpm git unzip postfix phantomjs

echo "Done installing the packages"
echo "----------------------------"
## Have to install node seperately otherwise things don't seem to work.
echo "Install Nodejs and NPM..."
sudo apt-get install --assume-yes nodejs npm
## Check if NPM is installed
echo "Make sure that NPM installed:"
echo "NPM version: "
npm -v

## Install Node version manager so we can have multiple versions of node installed
echo "Installing NVM..."
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash

echo "Enable mod rewrite and mod ssl:"
## enable rewrite, ssl, headers and fcgid for apache2.
a2enmod rewrite
a2enmod ssl
## for Content Security Policy (CSP).
a2enmod headers
## fcgid needed to run multiple versions of PHP under the same apache.
a2enmod fcgid

## make www-data use /bin/bash for shell
chsh -s /bin/bash www-data
## enable mcrypt
#sudo apt-get install php7.1-mcrypt
#sudo phpenmod mcrypt
echo "Install php-uploadprogress"
## add upload progress
sudo apt-get install --assume-yes php-uploadprogress

## Install Composer

## Changing to method the one composer.org
## Install 1.10.7 instead of 1.10.13 becasue latest breaks the base profile rebuild
EXPECTED_CHECKSUM="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"
if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
    >&2 echo 'ERROR: Invalid installer checksum'
    rm composer-setup.php
    exit 1
fi
php composer-setup.php --version=2.2.9 --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"

## Old way
#curl -sS https://getcomposer.org/installer | php
#mv composer.phar /usr/local/bin/composer
## Add Composer Global Bin To Path
echo "Add composer Global Bin to path"
printf "\nPATH=\"/home/vagrant/.config/composer/vendor/bin:\$PATH\"\n" | tee -a /home/vagrant/.profile

echo "Install Drush 8..."
## Add drush 8
wget https://github.com/drush-ops/drush/releases/download/8.4.6/drush.phar && \
    chmod +x drush.phar && \
    mv drush.phar /usr/local/bin/drush

## Install the drush registry_rebuild module
drush @none dl registry_rebuild-7.x


echo "Install Drush 9 and Drush 10 so they are available to us..."
## Install Drush 9.7.2 so we can use config-sync and migrate (install first so dependencies work out)
mkdir /home/vagrant/drush9
cd /home/vagrant/drush9 && composer require drush/drush:9.7.2
## Install latest Drush 10, currently Drush 10.4.0
mkdir /home/vagrant/drush10
cd /home/vagrant/drush10 && composer require drush/drush:10.x

## Install Drupal Console
echo "Install Drupal Console"
curl https://drupalconsole.com/installer -L -o drupal.phar
mv drupal.phar /usr/local/bin/drupal
chmod +x /usr/local/bin/drupal

## Install Codesniffer
echo "Install CodeSniffer"
composer global require "squizlabs/php_codesniffer"
## Config CodeSniffer to use Drupal coding standards
phpcs --config-set installed_paths /home/vagrant/.config/composer/vendor/drupal/coder/coder_sniffer

## Install PHP Mess Detector
echo "Install Drupal Mess Detector"
composer global require phpmd/phpmd

## Install Drupal Coder (seems to break if you don't specify a version).
echo "Install Drupal Coder"
composer global require drupal/coder

## Install phpcodesniffer-composer-installer to automagically add drupal coding standards to codesniffer
composer global require dealerdirect/phpcodesniffer-composer-installer

## Install Visual regression testing tool, Wraith
sudo npm install -g casperjs
gem install wraith
## Add environmental variable so wraith doesn't error out
printf "\nexport QT_QPA_PLATFORM=offscreen\n" | tee -a /home/vagrant/.profile
cd /var/www/html || exit
mkdir diff
cd diff || exit
wraith setup

## Move the .config file from root to vagrant home directory and adjust perms
mv /root/.config /home/vagrant/
chown -R vagrant:vagrant /home/vagrant/.config

## Change the PHP variables for Drupal
phpversions="${VGRTPHPDRUPAL7} ${VGRTPHPDRUPAL8}"
echo $phpversions
for phpversion in $phpversions
 do
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/$phpversion/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/$phpversion/fpm/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/$phpversion/cli/php.ini
sed -i "s/max_execution_time =.*/max_execution_time = 240/" /etc/php/$phpversion/apache2/php.ini
sed -i "s/max_execution_time =.*/max_execution_time = 240/" /etc/php/$phpversion/cli/php.ini
sed -i "s/max_input_time = .*/max_input_time = 240/" /etc/php/$phpversion/apache2/php.ini
sed -i "s/max_input_time = .*/max_input_time = 240/" /etc/php/$phpversion/cli/php.ini
sed -i "s/memory_limit = .*/memory_limit = -1/" /etc/php/$phpversion/apache2/php.ini
sed -i "s/memory_limit = .*/memory_limit = -1/" /etc/php/$phpversion/cli/php.ini
sed -i "s/memory_limit = .*/memory_limit = -1/" /etc/php/$phpversion/fpm/php.ini
sed -i "s/; max_input_vars = .*/max_input_vars = 10000/" /etc/php/$phpversion/apache2/php.ini
sed -i "s/; max_input_vars = .*/max_input_vars = 10000/" /etc/php/$phpversion/cli/php.ini
sed -i "s/;date.timezone = */date.timezone = America\/Toronto/" /etc/php/$phpversion/apache2/php.ini
sed -i "s/;date.timezone = */date.timezone = America\/Toronto/" /etc/php/$phpversion/cli/php.ini
done
## Set zend assertions to development setting so it doesn't throw errors in Drupal 8 testing
sed -i "s/zend.assertions = .*/zend.assertions = 1/" /etc/php/${VGRTPHPDRUPAL8}/apache2/php.ini
sed -i "s/zend.assertions = .*/zend.assertions = 1/" /etc/php/${VGRTPHPDRUPAL8}/cli/php.ini

## Set zend assertions to development setting so it doesn't throw errors in Drupal 8 testing
sed -i "s/zend.assertions = .*/zend.assertions = 1/" /etc/php/${VGRTPHPDRUPAL7}/apache2/php.ini
sed -i "s/zend.assertions = .*/zend.assertions = 1/" /etc/php/${VGRTPHPDRUPAL7}/cli/php.ini


## Add the include_path to the php.ini file so yarn works correctly in Drupal 8
echo "Adding include_path to PHP${VGRTPHPDRUPAL8} ..."
sed -i "s/;include_path = \"\.\:\/usr\/share\/php\"*/include_path = \".:\/usr\/share\/php:\/var\/www\/drupal8\/profiles\/uw_base_profile\"/" /etc/php/${VGRTPHPDRUPAL8}/cli/php.ini

echo "Adding include_path to PHP${VGRTPHPDRUPAL7} ..."
sed -i "s/;include_path = \"\.\:\/usr\/share\/php\"*/include_path = \".:\/usr\/share\/php:\/var\/www\/drupal8\/profiles\/uw_base_profile\"/" /etc/php/${VGRTPHPDRUPAL7}/cli/php.ini

## Add max_allowed_packet to my.cnf file so large sql dump files wouldn't timeout.
echo "[mysqld]
      max_allowed_packet = 100M" >> /etc/mysql/my.cnf

## Install xhprof
#pecl config-set preferred_state beta
#pecl install xhprof

## Add xhprof config
#echo "[xhprof]
#extension=xhprof.so
#xhprof.output_dir=/var/tmp/xhprof" > /etc/php/${VGRTPHPDRUPAL7}/mods-available/xhprof.ini

## Add xdebug config
# Enabled by default, if you need to disable add ; in front of zend_extension.
echo "zend_extension=xdebug.so
xdebug.mode=debug
xdebug.client_host=192.168.101.1
xdebug.client_port=9003
xdebug.max_nesting_level=256" > /etc/php/${VGRTPHPDRUPAL8}/mods-available/xdebug.ini

echo "zend_extension=xdebug.so
xdebug.mode=debug
xdebug.client_host=192.168.101.1
xdebug.client_port=9003
xdebug.max_nesting_level=256" > /etc/php/${VGRTPHPDRUPAL7}/mods-available/xdebug.ini

## Install the latest npm and gulp so we have them for responsive sites.
echo "Install gulp and bower"
sudo npm install npm@latest -g
## Drupal 8 currently needs gulp-cli 2.2.0 for Gesso (Jan. 2020)
sudo npm install gulp-cli@2.2.0 -g
sudo npm install gulp@4.0.2 -g
sudo npm install bower -g

## Create a symlink for node to nodejs.
#ln -s /usr/bin/nodejs /usr/bin/node

## Add the Servername to the apache2 conf file.
echo "ServerName ${vgrthostname}" >> /etc/apache2/apache2.conf

## Add fdsu sites to the hosts file
echo "
127.0.0.1       localhost
127.0.1.1       ${vgrthostname}
${vgrtip}  ${vgrtserver}7 ${vgrtserver}7alt ${vgrtserver}8 ${vgrtserver}8alt ${vgrtserver}8std ${vgrtserver}7rel ${vgrtserver}7lrel ${vgrtserver}7os
# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters" > /etc/hosts

#add the network interface .cfg file for eth1
echo "
auto eth1
iface eth1 inet static
address ${vgrtip}
netmask 255.255.255.0
network 192.168.101.1
broadcast 192.168.101.255" > /etc/network/interfaces.d/eth1.cfg

sudo chown -R vagrant:vagrant /home/vagrant

## Auto remove any files no longer needed.
apt-get autoremove --assume-yes

## Make PHP ${VGRTPHPDRUPAL7} the default, if not set PHP ${VGRTPHPDRUPAL8} is the default.
## update-alternatives --set php /usr/bin/php${VGRTPHPDRUPAL7}
update-alternatives --set php /usr/bin/php${VGDEFAULTPHPVERSION}

## Add the username and email we created with the setup.sh file to the .gitconfig file and move it to vagrant home folder.

git config --global user.name "${vgrtwatiamid}"
git config --global user.email ${vgrtemail}
## Set git to always rebase pulls to make code cleaner.
git config --global branch.autosetuprebase always

## Set git push to use the simple method by default.
git config --global push.default simple

## Set git hooks for all repositories.
git config --global core.hooksPath /home/vagrant/uw_wcms_tools/git_hooks/

mv /root/.gitconfig /home/vagrant/.gitconfig

## Add git autocomplete to build.
apt-get install -y git-core bash-completion

## Check what PHP is running during install
echo "Current PHP version"
php --version

echo ""
date
echo ""

echo "Installing chrome and chromedriver ..."
sudo curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add
sudo bash -c "echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' >> /etc/apt/sources.list.d/google-chrome.list"
sudo apt -y update
sudo apt -y install google-chrome-stable
echo "Google Chrome Version: "
google-chrome --version
cd
CHROME_DRIVER_VERSION=`curl -sS https://chromedriver.storage.googleapis.com/LATEST_RELEASE`
CHROME_DRIVER_FILE="https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip"
wget $CHROME_DRIVER_FILE
unzip chromedriver_linux64.zip
sudo cp chromedriver /usr/bin/chromedriver
sudo mv chromedriver /usr/local/bin/chromedriver
rm chromedriver_linux64.zip
echo "Done installing chrome and chromedriver"
